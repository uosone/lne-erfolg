<?php get_header(); ?>
<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature', array('disable_title' => true)); ?>
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'twentyfifteen'); ?></h1>
					</header>
					<!-- .page-header -->

					<div class="page-content">
						<p><?php _e('It looks like nothing was found at this location. Maybe try a search?', 'twentyfifteen'); ?></p>

						<?php get_search_form(); ?>
					</div>
					<!-- .page-content -->
				</section>
				<!-- .error-404 -->
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
