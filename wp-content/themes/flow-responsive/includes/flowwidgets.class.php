<?php

class FlowWidgetShowEntries extends WP_Widget{

	function __construct(){
		parent::__construct('flow_widget_show_entries', __('ShowEntries', 'flow-responsive'), array(
			'description' => __('Show Menuentries and last comments', 'flow-responsive'),
		));
	}

	/**
	 * @param array $options
	 *
	 * @return mixed
	 */
	public function getLastPosts($options = array()){

		$options = array_merge(array('number_of_entries' => 5), $options);

		$db = new WP_Query(apply_filters('widget_posts_args', array(
			'posts_per_page'      => $options['number_of_entries'],
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		)));

		$result = $db->get_posts();

		return $result;
	}



	/**
	 * @param array $options
	 *
	 * @return array
	 */
	public function getMenuEntries($options = array()){

		$options = array_merge(array('menu_id' => 0), $options);
		$result  = wp_get_nav_menu_items($options['menu_id']);

		if(!$result){
			return array();
		}

		$posts = array();

		foreach($result as $navEntry){

			if($navEntry->menu_item_parent != 0){
				continue;
			}

			$post = get_post($navEntry->object_id);

			if(!$post){
				continue;
			}

			$posts[] = $post;
		}

		return $posts;

	}

	/**
	 * @param array $options
	 *
	 * @return array
	 */
	public function getLastComments($options = array()){

		$options = array_merge(array('menu_id' => 0), $options);

		return array();
	}

	/**
	 * @param $result
	 *
	 * @return array
	 */
	public function prepareResult($result){

		$entries = array();

		foreach($result as $post){
			$txt                  = !empty($post->post_excerpt) ? $post->post_excerpt : $post->post_content;
			$entries[ $post->ID ] = array(
				'name'  => $post->post_name,
				'title' => $post->post_title,
				'href'  => get_permalink($post->ID),
				'image' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'),
				'text'  => FlowTemplateHelper::stripTheContent($txt, true, 30),
			);
		}

		return $entries;
	}


	/**
	 * @param array $args
	 * @param array $instance
	 */
	public function widget($args, $instance){

		$vars['title'] = $instance['title'];
		$vars['menu_id'] = !empty($instance['menu_id'])? $instance['menu_id']: 0;

		switch($instance['data_type']){

			case 'last_posts':
				$result = $this->getLastPosts(array('number_of_entries' => $instance['number_of_entries']));
				break;

			case 'menu_entries':
				$result = $this->getMenuEntries(array('menu_id' => $vars['menu_id']));
				break;

			case 'last_comments':
				$result = $this->getLastComments(array('number_of_entries' => $instance['number_of_entries']));
				break;

			default:
				return '';
				break;

		}

		if(empty($result)){
			return '';
		}

		$vars['entries'] = $this->prepareResult($result);
		echo FlowTemplateHelper::loadTemplate('widgets', 'show-entries-widget', $vars);
	}

	/**
	 * @param array $instance
	 */
	public function form($instance){

		$vars['this']      = $this;
		$vars['title']     = !empty($instance['title']) ? $instance['title'] : __('New title', 'flow-responsive');
		$vars['number']    = !empty($instance['number']) ? absint($instance['number']) : 5;
		$vars['data_type'] = !empty($instance['data_type']) ? $instance['data_type'] : 'last_posts';
		$vars['menu_id']      = !empty($instance['menu']) ? $instance['menu_id'] : 0;

		$vars['data_types'] = array();

		$vars['data_types']['last_posts']     = __('Last posts', 'flow-responsive');
		$vars['data_types']['menu_entries']   = __('Menu entries', 'flow-responsive');
		$vars['data_types']['last_comments']  = __('Last comments', 'flow-responsive');

		$menues = wp_get_nav_menus();

		foreach($menues as $menu){
			$vars['menues'][$menu->term_id] = $menu->name;
		}

		echo FlowTemplateHelper::loadTemplate('widgets', 'show-entries-form', $vars);
	}

	/**
	 * @param array $newInstance
	 * @param array $oldInstance
	 *
	 * @return array
	 */
	public function update($newInstance, $oldInstance){
		return array_merge($oldInstance, $newInstance);
	}

}

function wpb_load_widget(){
	register_widget('FlowWidgetShowEntries');
}

add_action('widgets_init', 'wpb_load_widget');
