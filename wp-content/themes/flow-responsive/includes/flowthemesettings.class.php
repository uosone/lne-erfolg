<?php

/**
 * Class FlowThemeSettings
 */
class FlowThemeSettings{

	/**
	 *
	 */
	public static function widgetsInit(){

		register_sidebar(array(
			'name' => 'Sidebar',
			'id'   => 'sidebar',
		));

		register_sidebar(array(
			'name' => 'Footer column 1',
			'id'   => 'footer_column_1',
		));

		register_sidebar(array(
			'name' => 'Footer column 2',
			'id'   => 'footer_column_2',
		));

		register_sidebar(array(
			'name' => 'Footer column 3',
			'id'   => 'footer_column_3',
		));

	}

	/**
	 *
	 */
	public static function themeOptionsInit(){
		register_setting('its_options', 'its_theme_options', 'FlowThemeSettings::validateThemeOptions');
	}

	/**
	 *
	 */
	public static function addThemePage(){
		add_theme_page('theme-options', 'Theme Options', 'edit_theme_options', 'theme-options', 'FlowThemeSettings::themeOptionsPage');
	}

	/**
	 * @return mixed|void
	 */
	public static function getThemeOptions(){

		$options = get_option('its_theme_options');

		empty($options['custom_javascript_code']) && $options['custom_javascript_code'] = '';
		empty($options['favicon']) && $options['favicon'] = '';
		empty($options['logo']) && $options['logo'];

		empty($options['slider_duration']) && $options['slider_duration'] = '4000';
		empty($options['slider_tab_navigation']) && $options['slider_tab_navigation'] = 'no';
		empty($options['slider_prev_next_navigation']) && $options['slider_prev_next_navigation'] = 'yes';

		empty($options['layout']) && $options['layout'] = 'boxed';
		empty($options['menu_sticky']) && $options['menu_sticky'] = 'no';
		empty($options['full_height']) && $options['full_height'] = 'no';

		return $options;
	}

	/**
	 *
	 */
	public static function themeOptionsPage(){
		echo FlowTemplateHelper::loadTemplate('adminsites', 'themeoptions', FlowFunctions::$themeOptions);
	}

	/**
	 * @param $input
	 *
	 * @return mixed
	 */
	public static function validateThemeOptions($input){

		!(is_numeric($input['slider_duration']) && ctype_digit($input['slider_duration'])) && $input['slider_duration'] = 4000;
		($input['layout'] != 'fullwidth') && $input['layout'] = 'boxed';
		($input['menu_sticky'] != 'yes') && $input['menu_sticky'] = 'no';
		($input['slider_tab_navigation'] != 'yes') && $input['slider_tab_navigation'] = 'no';
		($input['slider_prev_next_navigation'] != 'yes') && $input['slider_prev_next_navigation'] = 'no';

		return $input;
	}


	/**
	 *
	 */
	public static function addTaxonomyForObjectType(){
		register_taxonomy_for_object_type('category', 'page');
		register_taxonomy_for_object_type('category', 'slider');
		register_taxonomy_for_object_type('post_tag', 'page');
	}

	/**
	 *
	 */
	public static function registerCss(){

		if(is_admin()){
			return;
		}

		$themeUrl = get_template_directory_uri();

		wp_register_style('bootstrap-min-css', $themeUrl . '/vendor/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('bootstrap-min-css');

		wp_register_style('flow-style-css', $themeUrl . '/style.css');
		wp_enqueue_style('flow-style-css');
	}

	/**
	 *
	 */
	public static function registerJavascript(){

		if(is_admin()){
			return;
		}

		$themeUrl = get_template_directory_uri();

		wp_enqueue_script('jquery');

		wp_register_script('bootstrap-min-js', $themeUrl . '/vendor/bootstrap/js/bootstrap.min.js');
		wp_enqueue_script('bootstrap-min-js');

		wp_register_script('flow-ready-js', $themeUrl . '/javascript/ready.js');
		wp_enqueue_script(('flow-ready-js'));
	}

	/**
	 *
	 */
	public static function registerAdminJavascript(){

		$jsSrc = get_stylesheet_directory_uri() . '/javascript/';
		wp_register_script('flow_admin_ready', $jsSrc . 'admin-ready.js', array('jquery', 'jquery-ui-dialog'), false);
		wp_enqueue_script('flow_admin_ready');
		wp_enqueue_media();
	}

	/**
	 *
	 */
	public static function registerPostTypes(){
		register_post_type('slider', array(
			'label'    => 'Slider',
			'public'   => false,
			'show_ui'  => true,
			'supports' => array(
				'title',
				'editor',
				'featured',
				'thumbnail',
				'post-thumbnails',
				'custom-fields',
				'revisions'
			)
		));
	}

	/**
	 *
	 */
	public static function addMetaboxes(){

		add_meta_box('flow_slider_box', 'Select slider', 'FlowMetaboxes::showSlider', 'page', 'side', 'low');
		add_meta_box('flow_slider_box', 'Select slider', 'FlowMetaboxes::showSlider', 'post', 'side', 'low');

		add_meta_box('flow_banner_box', 'Banner options', 'FlowMetaboxes::showBanner', 'page', 'side', 'low');
		add_meta_box('flow_banner_box', 'Banner options', 'FlowMetaboxes::showBanner', 'post', 'side', 'low');

		add_meta_box('tagsdiv-post_tag', 'Page Tags', 'post_tags_meta_box', 'page', 'side', 'low');
		add_meta_box('categorydiv', 'Categories', 'post_categories_meta_box', 'page', 'side', 'core');

		add_meta_box('flow_single_templates_box', 'Single Templates', 'FlowMetaboxes::showSingleTemplate', 'post', 'side', 'low');

		//add_meta_box('flow_themesettings_box', 'Theme settings', 'FlowMetaboxes::showThemeSettings', 'page', 'side', 'low');
		//add_meta_box('flow_themesettings_box', 'Theme settings', 'FlowMetaboxes::showThemeSettings', 'post', 'side', 'low');

	}

	/**
	 *
	 */
	public static function afterSetupTheme(){
		register_nav_menus(array('main-menu' => 'Main Menu'));
	}

	/**
	 *
	 */
	public static function removeFromWpHead(){
		global $wp_widget_factory;
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		));
	}

	/**
	 *
	 */
	public static function addPostTypeSupport(){
		add_post_type_support('slider', 'category');
		add_post_type_support('slider', 'excerpt');
		add_post_type_support('page', 'category');
		add_post_type_support('page', 'excerpt');
	}


	/**
	 *
	 */
	public static function preGetPosts($query){
		if(empty($query->query['post_type']) && is_category() || is_tag() && empty($query->query_vars['suppress_filters'])){
			$query->set('post_type', array(
				'post',
				'nav_menu_item',
				'page'
			));

			return $query;
		}
	}

	/**
	 * @param $class
	 *
	 * @return mixed
	 */
	public function getAvatar($class){
		$class = str_replace("class='avatar", "class='author_gravatar circle ", $class);

		return $class;
	}

	/**
	 * @param $class
	 *
	 * @return string
	 */
	public function searchForm($class){
		$form = '<div class="flow-search-form"><form role="search" method="get" class="search-form" action="' . esc_url(home_url('/')) . '">
					<input type="search" class="form-control" placeholder="' . esc_attr_x('Search &hellip;', 'placeholder') . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x('Search for:', 'label') . '" />
			</form></div>';

		return $form;
	}

	/**
	 * @param $content
	 *
	 * @return string
	 */
	public function wrapContent($content){
	 return $content;
	}
}