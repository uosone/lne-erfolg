<?php

/**
 * Class FlowHelper
 */
class FlowHelper{

	/**
	 * @var array
	 */
	private static $metaCache = array();

	/**
	 * @param $postId
	 * @param $key
	 * @param $value
	 */
	static function setMeta($postId, $key, $value){
		$value = FlowHelper::preparePostValue($value);
		add_post_meta($postId, $key, $value, true) || update_post_meta($postId, $key, $value);
	}

	static function randomString($prefix = 'random'){
		return $prefix . md5(uniqid());
	}

	/**
	 * @param $postId
	 * @param $key
	 *
	 * @return mixed
	 */
	static function getMetaString($postId, $key){

		if(empty(self::$metaCache[$postId][$key])){
			self::$metaCache[$postId][$key] = get_post_meta($postId, $key, true);
		}
		return self::$metaCache[$postId][$key];
	}

	/**
	 * @param $postId
	 * @param $key
	 *
	 * @return mixed
	 */
	static function getMetaArray($postId, $key){
		if(empty(self::$metaCache[$postId][$key])){
			self::$metaCache[$postId][$key] = get_post_meta($postId, $key);
		}
		return self::$metaCache[$postId][$key];
	}

	/**
	 * @param $data
	 */
	static function debug($data, $config = array()){

		if(isset($config['headline'])){
			echo '<h1>' . $config['headline'] . '</h1>';
		}

		$debug = debug_backtrace();

		$headline = '';
		$headline .= !empty($debug[1]['class']) ? $debug[1]['class'] . $debug[1]['type'] : '';
		$headline .= !empty($debug[1]['function']) ? $debug[1]['function'] . ' ' : '';
		$headline .= $debug[0]['file'];
		$headline .= ' Line: ' . $debug[0]['line'];

		$style = !empty($config['display-none'])? 'display: none;': '';

		echo '<div class="debug"  style="'. $style .'" >';
		echo '<b>' . $headline . '</b>';
		echo '<pre>';
		echo var_dump($data);
		echo '</pre>';
		echo '</div>';
	}

	/**
	 * @param $value
	 *
	 * @return string
	 */
	static function preparePostValue($value){
		return $value;
	}

	/**
	 * @param $postTye
	 *
	 * @return array
	 */
	static function getCustomTemplates($postType){

		$dir = sanitize_html_class($postType) . '-templates' . DS;
		$dir = FlowFunctions::$templateDirectory . DS . $dir;

		if(!is_dir($dir)){
			return array();
		}

		$handle = opendir($dir);

		if(!$handle){
			return array();
		}

		$templates = array();

		while(($file = readdir($handle)) !== false){

			if(pathinfo($file, PATHINFO_EXTENSION) !== 'php'){
				continue;
			}

			if(strpos($file, 'template-') !== 0){
				continue;
			}

			$templateFile = $dir . $file;

			if(!preg_match('|' . $postType . '-template-name:(.*)$|mi', file_get_contents($templateFile), $header)){
				continue;
			}

			$file               = str_replace(array('.php', 'template-'), '', $file);
			$templates[ $file ] = trim($header[1]);

		}
		closedir($handle);

		return $templates;
	}

}