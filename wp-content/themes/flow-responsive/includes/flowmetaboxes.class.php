<?php

/**
 * Class FlowMetaboxes
 */
class FlowMetaboxes{

	/**
	 * @param $post
	 */
	static function showSingleTemplate($post){

		$vars = array();

		$vars['selected_template'] = FlowHelper::getMetaString($post->ID, 'selected_template');
		empty($vars['selected_template']) && $vars['selected_template'] = 'default';
		$vars['templates'] = FlowHelper::getCustomTemplates('single');
		asort($vars['templates']);

		echo FlowTemplateHelper::loadTemplate('metaboxes', 'single-templates', $vars);
	}

	/**
	 * @param $postId
	 */
	static function saveSingleTemplate($postId){

		$value = (!empty($_POST['flow_selected_template'])) ? $_POST['flow_selected_template'] : 'default';
		FlowHelper::setMeta($postId, 'selected_template', $value);
	}

	/**
	 * @param $post
	 */
	static function showBanner($post){

		$vars = array();

		$vars['headline']        = FlowHelper::getMetaString($post->ID, 'banner_headline');
		$vars['image']           = FlowHelper::getMetaString($post->ID, 'banner_image');
		$vars['text']            = FlowHelper::getMetaString($post->ID, 'banner_text');
		$vars['disable_title']   = FlowHelper::getMetaString($post->ID, 'banner_disable_title');
		$vars['disable_content'] = FlowHelper::getMetaString($post->ID, 'banner_disable_content');
		$vars['animation']       = FlowHelper::getMetaString($post->ID, 'banner_animation');

		echo FlowTemplateHelper::loadTemplate('metaboxes', 'banner', $vars);
	}

	/**
	 * @param $postId
	 */
	static function saveBanner($postId){

		$value = (!empty($_POST['banner']['headline'])) ? $_POST['banner']['headline'] : '';
		FlowHelper::setMeta($postId, 'banner_headline', $value);

		$value = (!empty($_POST['banner']['image'])) ? $_POST['banner']['image'] : '';
		FlowHelper::setMeta($postId, 'banner_image', $value);

		$value = (!empty($_POST['banner']['text'])) ? $_POST['banner']['text'] : '';
		FlowHelper::setMeta($postId, 'banner_text', $value);

		$value = (!empty($_POST['banner']['disable_title'])) ? $_POST['banner']['disable_title'] : '';
		FlowHelper::setMeta($postId, 'banner_disable_title', $value);

		$value = (!empty($_POST['banner']['disable_content'])) ? $_POST['banner']['disable_content'] : '';
		FlowHelper::setMeta($postId, 'banner_disable_content', $value);

		$value = (!empty($_POST['banner']['animation'])) ? $_POST['banner']['animation'] : '';
		FlowHelper::setMeta($postId, 'banner_animation', $value);

	}

	/**
	 * @param $post
	 */
	static function showSlider($post){

		$vars          = array();
		$vars['value'] = FlowHelper::getMetaString($post->ID, 'slider');

		$sliderIdsArray = explode(',', $vars['value']);

		$vars['slider'] = FlowTemplateHelper::getSliderArray();

		foreach($vars['slider'] as $key => $value){

			if(in_array($value['id'], $sliderIdsArray)){
				$vars['slider'][ $key ]['selected'] = true;
			}
			else{
				$vars['slider'][ $key ]['selected'] = false;
			}
		}

		echo FlowTemplateHelper::loadTemplate('metaboxes', 'slider', $vars);
	}

	/**
	 * @param $postId
	 */
	static function saveSlider($postId){

		$value = (!empty($_POST['flow_slider'])) ? $_POST['flow_slider'] : '';
		FlowHelper::setMeta($postId, 'slider', $value);
	}

	/**
	 * @param $post
	 */
	static function showThemeSettings($post){

		$vars = array();

		$vars['theme_settings'] = FlowHelper::getMetaString($post->ID, 'theme_settings');

		if(empty($vars['theme_settings']['full_width_section'])){
			$vars['theme_settings']['full_width_section'] = 0;
		}

		echo FlowTemplateHelper::loadTemplate('metaboxes', 'theme-settings', $vars['theme_settings']);

	}

	/**
	 * @param $postId
	 */
	static function saveThemeSettings($postId){

		$themeSettings = $_POST['theme_settings'];

		if(empty($themeSettings['wrap_sections'])){
			$themeSettings['wrap_sections'] = 0;
		}

		FlowHelper::setMeta($postId, 'theme_settings', $themeSettings);
	}

	/**
	 * @param $post
	 */
	static function showSliderOptions($post){

		$vars = array();

		$vars['disable_title']   = FlowHelper::getMetaString($post->ID, 'disable_title');
		$vars['disable_content'] = FlowHelper::getMetaString($post->ID, 'disable_content');
		$vars['animation']       = FlowHelper::getMetaString($post->ID, 'animation');

		echo FlowTemplateHelper::loadTemplate('metaboxes', 'slider-options', $vars);
	}

	/**
	 * @param $postId
	 */
	static function saveSliderOptions($postId){

		$value = (!empty($_POST['slider_options']['disable_title'])) ? $_POST['slider_options']['disable_title'] : '';
		FlowHelper::setMeta($postId, 'disable_title', $value);

		$value = (!empty($_POST['slider_options']['disable_content'])) ? $_POST['slider_options']['disable_content'] : '';
		FlowHelper::setMeta($postId, 'disable_content', $value);

		$value = (!empty($_POST['slider_options']['animation'])) ? $_POST['slider_options']['animation'] : '';
		FlowHelper::setMeta($postId, 'animation', $value);
	}

}