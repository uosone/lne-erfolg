<?php

/**
 * Class FlowTemplateHelper
 */
class FlowTemplateHelper
{

    static function getAnimationOptions($selected = '')
    {
        $animations = array(
            'bounce',
            'flash',
            'pulse',
            'rubberBand',
            'shake',
            'swing',
            'tada',
            'wobble',
            'jello',
            'bounceIn',
            'bounceInDown',
            'bounceInLeft',
            'bounceInRight',
            'bounceInUp',
            'bounceOut',
            'bounceOutDown',
            'bounceOutLeft',
            'bounceOutRight',
            'bounceOutUp',
            'fadeIn',
            'fadeInDown',
            'fadeInDownBig',
            'fadeInLeft',
            'fadeInLeftBig',
            'fadeInRight',
            'fadeInRightBig',
            'fadeInUp',
            'fadeInUpBig',
            'fadeOut',
            'fadeOutDown',
            'fadeOutDownBig',
            'fadeOutLeft',
            'fadeOutLeftBig',
            'fadeOutRight',
            'fadeOutRightBig',
            'fadeOutUp',
            'fadeOutUpBig',
            'flipInX',
            'flipInY',
            'flipOutX',
            'flipOutY',
            'lightSpeedIn',
            'lightSpeedOut',
            'rotateIn',
            'rotateInDownLeft',
            'rotateInDownRight',
            'rotateInUpLeft',
            'rotateInUpRight',
            'rotateOut',
            'rotateOutDownLeft',
            'rotateOutDownRight',
            'rotateOutUpLeft',
            'rotateOutUpRight',
            'hinge',
            'rollIn',
            'rollOut',
            'zoomIn',
            'zoomInDown',
            'zoomInLeft',
            'zoomInRight',
            'zoomInUp',
            'zoomOut',
            'zoomOutDown',
            'zoomOutLeft',
            'zoomOutRight',
            'zoomOutUp',
            'slideInDown',
            'slideInLeft',
            'slideInRight',
            'slideInUp',
            'slideOutDown',
            'slideOutLeft',
            'slideOutRight',
            'slideOutUp'
        );
        $re = '';
        foreach ($animations as $animation) {
            $select = '';
            if ($animation == $selected) {
                $select = ' selected="' . $selected . '" ';
            }
            $re .= '<option ' . $select . ' value="' . $animation . '">' . $animation . '</option>' . PHP_EOL;
        }
        return $re;
    }

    /**
     * @param null $postId
     *
     * @return array
     */
    static function getSliderArray($postId = null, $categoryId = null)
    {

        if ($postId) {

            $sliderIds = FlowHelper::getMetaString($postId, 'slider');
            $sliderIds = explode(',', $sliderIds);
            $slider = get_posts(array(
                'post_type' => 'slider',
                'include' => $sliderIds,
            ));

            $sortSlider = array();

            foreach ($sliderIds as $valueId) {
                foreach ($slider as $key => $slide) {
                    if ($slide->ID == $valueId) {
                        $sortSlider[] = $slide;
                    }
                }
            }

            $slider = $sortSlider;
        } else if ($categoryId) {
            $slider = get_posts(array(
                'category' => $categoryId,
                'post_status' => 'publish',
                'post_type' => 'slider',
                'orderby' => 'date',
                'order' => 'desc',
                'numberposts' => 200
            ));
        } else {
            $slider = get_posts(array(
                'post_type' => 'slider',
                'numberposts' => 200,
            ));
        }

        $sliderArray = array();

        foreach ($slider as $slide) {
            $sliderArray[] = self::prepareSlidePost($slide);
        }

        return $sliderArray;
    }

    /**
     * @param $slide
     *
     * @return array
     */
    public static function prepareSlidePost($slide)
    {

        $image = wp_get_attachment_image_src(get_post_thumbnail_id($slide->ID), 'full');
        $image = $image[0];

        $animation = FlowHelper::getMetaString($slide->ID, 'animation');

        if (empty($animation) || $animation == '') {
            $animation = FlowFunctions::$themeOptions['slider_animation'];
        }

        $re = array(
            'title' => $slide->post_title,
            'content' => $slide->post_content,
            'excerpt' => $slide->post_excerpt,
            'id' => $slide->ID,
            'image' => $image,
            'animation' => $animation,
            'link' => FlowHelper::getMetaString($slide->ID, 'link'),
            'disable_title' => FlowHelper::getMetaString($slide->ID, 'disable_title'),
            'disable_content' => FlowHelper::getMetaString($slide->ID, 'disable_content'),
        );

        return $re;
    }

    /**
     * @param $slider
     *
     * @return array
     */
    public static function buildSlider($slider, $options = array())
    {

        ($options == null) && $options = array();

        $options = array_merge(array(
            'id' => FlowHelper::randomString(),
            'duration' => FlowFunctions::$themeOptions['slider_duration'],
            'tab_navigation' => FlowFunctions::$themeOptions['slider_tab_navigation'],
            'prev_next_links' => FlowFunctions::$themeOptions['slider_prev_next_navigation'],
            'type' => 'image',
        ), $options);

        return FlowTemplateHelper::loadTemplate('slider', 'default', array('slider' => $slider, 'options' => $options));
    }

    /**
     * @param $folder
     * @param $template
     * @param array $vars
     */
    static function loadTemplate($folder, $template, $vars = array())
    {
        ob_start();
        require FlowFunctions::$templateDirectory . DS . 'templates' . DS . $folder . DS . $template . '.php';
        $content = ob_get_contents();
        ob_end_clean();
        return apply_filters('flow_load_template_' . $folder . '_' . $template, $content, $vars);
    }


    /**
     * @param $templateName
     *
     * @return bool|string
     */
    static function loadSingleTemplate($templateName)
    {

        $template = FlowFunctions::$templateDirectory . DS . 'single-templates' . DS . 'template-' . $templateName . '.php';

        if (!file_exists($template)) {
            return false;
        }

        ob_start();
        require $template;
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * @param $text
     * @param bool $force
     * @param int $length
     * @param null $href
     *
     * @return string
     * @todo
     */
    public static function stripTheContent($text, $force = false, $length = 250)
    {

        $text = strip_shortcodes($text);

        if (is_category() || is_tag() || is_home() || $force) {

            if (strlen($text) < $length + 10) {
                return strip_tags($text);
            }

            $breakPos = strpos($text, ' ', $length);
            $visible = substr($text, 0, $breakPos);

            return strip_tags(balanceTags($visible));
        } else {

            return strip_tags($text);
        }

    }

    /**
     * @param null $query
     */
    static function getPaginationList($query = null)
    {

        global $wp_query;

        $query = $query ? $query : $wp_query;

        $getPaged = get_query_var('paged');

        $paginate = paginate_links(array(
            'type' => 'array',
            'total' => $query->max_num_pages,
            'format' => '?paged=%#%',
            'current' => max(1, $getPaged),
            'prev_text' => __('&laquo;', 'flow-responsive'),
            'next_text' => __('&raquo;', 'flow-responsive'),
        ));


        if ($query->max_num_pages <= 1) {
            return '';
        }

        $return = '<ul class="pagination">';

        foreach ($paginate as $key => $page) {
            $class = ($getPaged == $key) ? 'active' : 'disabled';
            $return .= '<li class="' . $class . '">' . $page . '</li>';
        }

        $return .= '</ul>';

        $return = '<nav class="flow-pagination-list">' . $return . '</nav>';

        return $return;
    }

    /**
     * @return string
     */
    public static function getPaginationPreviousNextLinks()
    {

        return '<nav class="flow-pagination-previous-next-links">
		       <ul class="pager">
				<li >' . get_previous_post_link('%link', '&laquo; %title', true) . '</li>
				<li>' . get_next_post_link('%link', '%title &raquo;', true) . '</li>
				</ul>
			   </nav>';
    }

    /**
     *
     */
    public static function getComments()
    {
        if (comments_open() || get_comments_number()) {
            comments_template();
        }
    }

    /**
     * @param $comment
     * @param $args
     * @param $depth
     */
    public static function getComment($comment, $args, $depth)
    {

        echo FlowTemplateHelper::loadTemplate('blocks', 'comment', array(
            'comment' => $comment,
            'args' => $args,
            'depth' => $depth
        ));
    }

    public static function buildVideoFrame($code, $config = array())
    {
	if($config['platform'] == 'vimeo'){
        return '<div class="responsive-video vimeo"><iframe src="//player.vimeo.com/video/' . $code . '" width="1600" height="900" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
	}
        return '<div class="responsive-video youtube"><iframe width="1600" height="900"  src="//www.youtube.com/embed/' . $code . '" frameborder="0"></iframe></div>';
    }

    public static function addJavascriptFooter($content = '')
    {
        FlowFunctions::$globals['js_footer'][] = $content;
    }

    public static function getJavascriptFooter()
    {
        return implode(PHP_EOL, FlowFunctions::$globals['js_footer']);
    }
}
