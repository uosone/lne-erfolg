<?php

/**
 * Class FlowShortcodes
 */
class FlowShortcodes{

	/**
	 * @param $args
	 * @param string $start
	 *
	 * @return string
	 */
	private static function buildCssClass($args, $start = ''){

		$class = $start;

		(!empty($args['class'])) && $class .= ' ' . $args['class'];

		return $class;
	}

	/**
	 * @param $ar
	 * @param $content
	 *
	 * @return bool|string
	 */
	static function buildStringFromShortcodeArray($ar, $content){

		$contentLength = strlen($content);

		if(!is_array($ar) && ($content === false || $contentLength < 1)){
			return false;
		}
		else if($contentLength > 0){
			$ar['content'] = do_shortcode($content);
		}

		$ar = json_encode($ar) . ',';

		if(!$ar){
			return false;
		}

		return $ar;
	}

	/**
	 * @param $content
	 *
	 * @return array
	 */
	static function getArrayFromShortcode($args){

		$tags = do_shortcode($args);
		$tags = explode('},', $tags);

		$re = array();

		foreach($tags as $value){
			$value = substr($value, strpos($value, '{')) . '}';
			($json = json_decode($value, true)) && $re[] = $json;

		}

		return $re;
	}

	/**
	 * @param $args
	 * @param $content
	 */
	static function slider($args, $content){

		$class  = self::buildCssClass($args, 'contentslider');
		$slider = self::getArrayFromShortcode($content);

		if(!$slider){
			return '';
		}

		$slider = FlowTemplateHelper::buildSlider($slider, $args);

		if(!$slider){
			return '';
		}

		return $slider;

	}

	/**
	 * @param $args
	 * @param $content
	 */
	static function slide($args, $content = false){

		$args['class'] = self::buildCssClass($args, '');

		if(!empty($args['post_id'])){

			$slide = get_post($args['post_id']);

			if(!$slide){
				return '';
			}

			(strlen($content) < 1 || $content == false) && $content = $slide->post_content;

			$slide = FlowTemplateHelper::prepareSlidePost($slide);

			(empty($args['title'])) && $args['title'] = $slide['title'];
			(empty($args['excerpt'])) && $args['excerpt'] = $slide['excerpt'];
			(empty($args['id'])) && $args['id'] = $slide['id'];
			(empty($args['image'])) && $args['image'] = $slide['image'];
			(empty($args['link'])) && $args['link'] = $slide['link'];

		}

		return self::buildStringFromShortcodeArray($args, $content);
	}

	/**
	 * @param $args
	 * @param $content
	 *
	 * @return string
	 */
	static function section($args, $content = false){

		$class = self::buildCssClass($args, 'section');

		if(!empty($args['post_id'])){

			$post = get_post($args['post_id']);

			if(!$post){

				return '';
			}

			($content == false && strlen($content) < 1) && $content = $post->post_content;

			if(!empty($args['type']) && $args['type'] == 'preview'){

				empty($args['headline']) && $args['headline'] = $post->post_title;

				if($image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full')){
					empty($args['image_url']) && $args['image_url'] = $image[0];
					$args['image_class'] = !empty($args['image_class']) ? $args['image_class'] : 'circle';
				}
				empty($args['url']) && $args['url'] = get_permalink($post->ID);
			}
		}

		if(!empty($args['youtube_code'])){

			($content == false) && $content = '';
			$content .= FlowTemplateHelper::buildVideoFrame($args['youtube_code']);
		}

		$style = '';

		if(!empty($args['style'])){
			$style = $args['style'];
		}

		if(!empty($args['background'])){
			$style .= ' background-image:url(\'' . $args['background'] . '\');';
			$class .= ' section-background';
		}

		$re = '<div class="' . $class . '" style="' . $style . '"><div ' . (!empty($args['animation']) ? 'data-animation="' . $args['animation'] . '"' : '') . ' class="container">' . ((!empty($args['headline'])) ? '<div class="section-headline"><h2>' . $args['headline'] . '</h2></div>' : '') . ((!empty($args['subline'])) ? '<h3 class="section-subline">' . $args['subline'] . '</h3>' : '') . do_shortcode($content) . '</div></div>';

		return $re;

	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return string
	 */
	static function youtube($args, $content = false){

		if(empty($args['code'])){
			return '';
		}

		return FlowTemplateHelper::buildVideoFrame($args['code']);
	}

	/**
	 * @param $args
	 *
	 * @return string
	 */
	static function testimonial($args){
		$args['no_href'] = true;

		return self::boxes($args);
	}


	/**
	 * @param $args
	 *
	 * @return bool|string
	 */
	static function boxes($args){

		$vars  = array();
		$boxes = array();
		$ids   = array();

		$source   = !empty($args['source']) ? $args['source'] : 'ids';
		$template = !empty($args['template']) ? $args['template'] : 'list';

		switch($source){

			case 'ids':

				if(empty($args['ids'])){
					return false;
				}

				$ids = is_string($args['ids']) ? explode(',', $args['ids']) : $args['ids'];

				break;

			case 'category':

				$orderBy     = !empty($args['order_by']) ? $args['order_by'] : 'date';
				$order       = !empty($args['order']) ? $args['order'] : 'DESC';
				$numberPosts = !empty($args['number_posts']) ? $args['number_posts'] : '12';

				$posts = get_posts(array(
					'category'    => get_cat_ID($args['name']),
					'post_status' => 'publish',
					'post_type'   => array('page', 'post', 'testimonials'),
					'orderby'     => $orderBy,
					'order'       => $order,
					'numberposts' => $numberPosts,
				));

				if(!$posts || count($posts) == 0){
					return false;
				}

				foreach($posts as $post){
					$ids[] = $post->ID;
				}

				if(!empty($args['show_all_link']) && $args['show_all_link'] == 'yes'){
					$vars['show_all_link'] = get_category_link($args['id']);
				}

				break;
			default:
				return false;
				break;
		}

		if(empty($ids)){
			return false;
		}

		foreach($ids as $id){

			$post = get_post($id);

			$boxes[ $id ] = array(
				'name'    => $post->post_name,
				'href'    => (!empty($args['no_href']) && $args['no_href'] == true) ? false : get_permalink($post->ID),
				'title'   => $post->post_title,
				'content' => $post->post_content,
				'excerpt' => $post->post_excerpt,
				'image'   => wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full')
			);

		}

		$cssId    = !empty($args['css_id']) ? $args['css_id'] : '';
		$class    = !empty($args['css_class']) ? $args['css_class'] : '';
		$imgClass = !empty($args['image_class']) ? $args['image_class'] : 'circle';

		if(!empty($args['filter']) && $args['filter'] == 'yes'){
			foreach($ids as $id){
				$tags = wp_get_post_tags($id);
				if(!empty($tags)){
					$boxes[ $id ]['tags'] = '';
					foreach($tags as $tag){
						$boxes[ $id ]['tags'] .= $tag->slug . ',';
						$vars['tags'][ $tag->slug ] = $tag->name;
					}
					$boxes[ $id ]['tags'] = rtrim($boxes[ $id ]['tags'], ",");
				}
			}
			empty($cssId) && $cssId = FlowHelper::randomString();
		}

		switch($template){

			case 'list':
				$vars['boxes']       = $boxes;
				$vars['id']          = $cssId;
				$vars['class']       = $class;
				$vars['image_class'] = $imgClass;
				$vars['length']      = !empty($args['length']) ? $args['length'] : 400;

				return FlowTemplateHelper::loadTemplate('shortcodes', 'list', $vars);
				break;

			case 'mouse_over_boxes':
			case 'columns':
			case 'columns-horizontal':
			case 'columns-pricing':
				$col        = array();
				$col['std'] = !empty($args['col']) ? $args['col'] : '12';
				$col['lg']  = !empty($args['col_lg']) ? $args['col_lg'] : $col['std'];
				$col['md']  = !empty($args['col_md']) ? $args['col_md'] : $col['std'];
				$col['sm']  = !empty($args['col_sm']) ? $args['col_sm'] : $col['std'];
				$col['xs']  = !empty($args['col_xs']) ? $args['col_xs'] : $col['std'];

				$cols = 'col-xs-' . $col['xs'] . ' col-sm-' . $col['sm'] . ' col-md-' . $col['md'] . ' col-lg-' . $col['lg'];

				$vars['max']         = 12 / $col['lg'];
				$vars['boxes']       = $boxes;
				$vars['id']          = $cssId;
				$vars['class']       = $class;
				$vars['image_class'] = $imgClass;
				$vars['cols']        = $cols;
				$vars['length']      = !empty($args['length']) ? $args['length'] : 100;

				return FlowTemplateHelper::loadTemplate('shortcodes', $template, $vars);

				break;

			case 'accordions':
			case 'tabs':
				$vars['id']                = !empty($args['id']) ? $args['id'] : FlowHelper::randomString();
				$vars['class']             = self::buildCssClass($args, '');
				$vars[ $args['template'] ] = $boxes;

				return FlowTemplateHelper::loadTemplate('shortcodes', $args['template'], $vars);

				break;
		}
	}

	/**
	 * @param $args
	 * @param $content
	 */
	static function tabs($args, $content){

		$vars          = array();
		$vars['id']    = !empty($args['id']) ? $args['id'] : FlowHelper::randomString();
		$vars['class'] = self::buildCssClass($args, '');
		$vars['tabs']  = self::getArrayFromShortcode($content);

		return FlowTemplateHelper::loadTemplate('shortcodes', 'tabs', $vars);
	}

	/**
	 * @param $args
	 * @param $content
	 *
	 * @return bool|string
	 */
	static function tab($args, $content){

		$vars['class'] = self::buildCssClass($args, '');

		if(empty($args['post_id'])){
			$vars['title'] = !empty($args['title']) ? $args['title'] : '';
		}

		else{

			$content = trim($content);
			$post    = get_post($args['post_id'], ARRAY_A);

			if(!$post){
				return '';
			}

			$vars['title'] = empty($args['title']) ? $post['post_title'] : $args['title'];
			$content       = empty($content) ? $post['post_content'] : $content;

		}

		if(!empty($args['youtube_code'])){
			$content .= FlowTemplateHelper::buildVideoFrame($args['youtube_code']);
		}

		return self::buildStringFromShortcodeArray($vars, $content);
	}

	/**
	 * @param $args
	 * @param $content
	 *
	 * @return string
	 */
	static function modal($args, $content){

		$vars          = array();
		$vars['class'] = '';

		$content = do_shortcode($content);

		if(empty($args['post_id'])){

			$vars['title'] = !empty($args['title']) ? $args['title'] : '';


			if(!empty($args['vimeo_code'])){
				$vars['class']   = ' video-modal';
				$vars['content'] = FlowTemplateHelper::buildVideoFrame($args['vimeo_code'], array('platform' => 'vimeo'));
				$vars['video_modal'] = true;
			}
			else if(empty($args['youtube_code'])){
				$vars['class']   = ' video-modal';
				$vars['content'] = FlowTemplateHelper::buildVideoFrame($args['youtube_code']);
				$vars['video_modal'] = true;
			}
			else{
				$vars['content'] = $content;
			}
		}

		else{

			$content = trim($content);

			$post = get_post($args['post_id'], ARRAY_A);

			if(!$post){
				return '';
			}

			$vars['title']   = !empty($args['title']) ? $args['title'] : $post['post_title'];
			$vars['content'] = !empty($content) ? $content : $post['post_content'];

		}

		$vars['id']   = !empty($args['id']) ? $args['id'] : FlowHelper::randomString();
		$vars['size'] = !empty($args['size']) && $args['size'] == 'large' ? 'modal-lg' : '';
		$vars['class'] .= self::buildCssClass($args, '');
		$vars['link_class']         = !empty($args['link_class']) ? $args['link_class'] : 'btn btn-primary';
		$vars['wrapper_link_class'] = !empty($args['wrapper_link_class']) ? $args['wrapper_link_class'] : 'align-left';
		$vars['modal_title'] = !empty($args['modal_title'])? $args['modal_title'] : $args['title'];
		$vars['link_text'] = !empty($args['link_text']) ? $args['link_text'] : $vars['title'];

		return FlowTemplateHelper::loadTemplate('shortcodes', 'modal', $vars);

	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return string
	 */
	public static function accordions($args, $content = false){

		$vars               = array();
		$vars['id']         = !empty($args['id']) ? $args['id'] : FlowHelper::randomString();
		$vars['class']      = self::buildCssClass($args, '');
		$vars['accordions'] = self::getArrayFromShortcode($content);

		return FlowTemplateHelper::loadTemplate('shortcodes', 'accordions', $vars);
	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return bool|string
	 */
	public static function accordion($args, $content = false){

		$vars['class'] = self::buildCssClass($args, '');

		if(empty($args['post_id'])){
			$vars['title'] = !empty($args['title']) ? $args['title'] : '';
		}

		else{

			$content = trim($content);
			$post    = get_post($args['post_id'], ARRAY_A);

			if(!$post){
				return '';
			}

			$vars['title'] = empty($args['title']) ? $post['post_title'] : $args['title'];
			$content       = empty($content) ? $post['post_content'] : $content;

		}

		if(!empty($args['youtube_code'])){
			$content .= FlowTemplateHelper::buildVideoFrame($args['youtube_code']);
		}

		return self::buildStringFromShortcodeArray($vars, $content);
	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return string
	 */
	public static function code($args, $content = false){
		return '<pre>' . htmlspecialchars($content) . '</pre>';
	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return string
	 */
	public static function columns($args, $content = false){
		$class = self::buildCssClass($args, 'columns-shortcode');

		return '<div class="container ' . $class . '"><div class="row">' . do_shortcode($content) . '</div></div>';
	}

	/**
	 * @param $args
	 * @param bool|false $content
	 *
	 * @return string
	 */
	public static function column($args, $content = false){

		$col        = array();
		$col['std'] = !empty($args['col']) ? $args['col'] : '12';
		$col['lg']  = !empty($args['col_lg']) ? $args['col_lg'] : $col['std'];
		$col['md']  = !empty($args['col_md']) ? $args['col_md'] : $col['lg'];
		$col['sm']  = !empty($args['col_sm']) ? $args['col_sm'] : $col['md'];
		$col['xs']  = !empty($args['col_xs']) ? $args['col_xs'] : $col['sm'];

		return '<div class="col-xs-' . $col['xs'] . ' col-sm-' . $col['sm'] . ' col-md-' . $col['md'] . ' col-lg-' . $col['lg'] . '">' . do_shortcode($content) . '</div>';
	}

}
