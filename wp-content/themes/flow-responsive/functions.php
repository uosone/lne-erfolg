<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

/**
 * @TODO - Fehleranzeige entfernen
 */
error_reporting(E_ALL);
ini_set('display_errors', 'on');

/**
 * Class FlowFunctions
 *
 */
class FlowFunctions{

	/**
	 * @var
	 */
	public static $templateDirectory;

	/**
	 * @var
	 */
	public static $themeOptions;

	/**
	 * @var
	 */
	public static $globals;


	/**
	 *
	 */
	static function init(){

		self::$templateDirectory = get_template_directory();

		self::$globals = array(
			'show_slider' => false,
			'js_footer' => array(),
		);

		require_once self::$templateDirectory . DS . 'vendor' . DS . 'bootstrap_navwalker.php';
		require_once self::$templateDirectory . DS . 'includes' . DS . 'flowshortcodes.class.php';
		require_once self::$templateDirectory . DS . 'includes' . DS . 'flowtemplatehelper.class.php';
		require_once self::$templateDirectory . DS . 'includes' . DS . 'flowhelper.class.php';
		require_once self::$templateDirectory . DS . 'includes' . DS . 'flowmetaboxes.class.php';
		require_once self::$templateDirectory . DS . 'includes' . DS . 'flowwidgets.class.php';

		self::$themeOptions = FlowFunctions::getThemeOptions();

		add_action('admin_init', 'FlowFunctions::addMetaboxes');
		add_action('admin_init', 'FlowFunctions::addPostTypeSupport');
		add_action('admin_init', 'FlowFunctions::themeOptionsInit');
		add_action('admin_menu', 'FlowFunctions::addThemePage');
		add_action('init', 'FlowFunctions::registerVendorCss',1);
		add_action('init', 'FlowFunctions::registerCss',2);
		add_action('init', 'FlowFunctions::registerJavascript');
		add_action('init', 'FlowFunctions::registerPostTypes');
		add_action('init', 'FlowFunctions::addTaxonomyForObjectType');
		add_action('after_setup_theme', 'FlowFunctions::afterSetupTheme');
		add_action('admin_enqueue_scripts', 'FlowFunctions::registerAdminJavascript');
		add_action('save_post', 'FlowMetaboxes::saveSlider');
		add_action('save_post', 'FlowMetaboxes::saveBanner');
		add_action('save_post', 'FlowMetaboxes::saveSliderOptions');
		add_action('save_post', 'FlowMetaboxes::saveSingleTemplate');
		//add_action('save_post', 'FlowMetaboxes::saveThemeSettings');
		add_action('widgets_init', 'FlowFunctions::widgetsInit');

		add_filter('pre_get_posts', 'FlowFunctions::preGetPosts');
		add_filter('widget_text', 'do_shortcode');
		add_filter('get_avatar', 'FlowFunctions::getAvatar');
		add_filter('get_search_form', 'FlowFunctions::searchForm');

		add_shortcode('boxes', 'FlowShortcodes::boxes');
		add_shortcode('testimonial', 'FlowShortcodes::testimonial');
		add_shortcode('section', 'FlowShortcodes::section');
		add_shortcode('youtube', 'FlowShortcodes::youtube');
		add_shortcode('slider', 'FlowShortcodes::slider');
		add_shortcode('slide', 'FlowShortcodes::slide');
		add_shortcode('modal', 'FlowShortcodes::modal');
		add_shortcode('tabs', 'FlowShortcodes::tabs');
		add_shortcode('tab', 'FlowShortcodes::tab');
		add_shortcode('accordions', 'FlowShortcodes::accordions');
		add_shortcode('accordion', 'FlowShortcodes::accordion');
		add_shortcode('code', 'FlowShortcodes::code');
		add_shortcode('columns', 'FlowShortcodes::columns');
		add_shortcode('column', 'FlowShortcodes::column');

		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(200, 200);

		remove_filter('the_content', 'wpautop');
		remove_filter('the_excerpt', 'wpautop');

	}


	/**
	 *
	 */
	public static function widgetsInit(){

		register_sidebar(array(
			'name' => 'Sidebar',
			'id'   => 'sidebar',
		));

		register_sidebar(array(
			'name' => 'Footer column 1',
			'id'   => 'footer_column_1',
		));

		register_sidebar(array(
			'name' => 'Footer column 2',
			'id'   => 'footer_column_2',
		));

		register_sidebar(array(
			'name' => 'Footer column 3',
			'id'   => 'footer_column_3',
		));

	}

	/**
	 *
	 */
	public static function themeOptionsInit(){
		register_setting('its_options', 'its_theme_options', 'FlowFunctions::validateThemeOptions');
	}

	/**
	 *
	 */
	public static function addThemePage(){
		add_theme_page('theme-options', 'Theme Options', 'edit_theme_options', 'theme-options', 'FlowFunctions::themeOptionsPage');
	}

	/**
	 * @return mixed|void
	 */
	public static function getThemeOptions(){

		$options = get_option('its_theme_options');

		empty($options['favicon']) && $options['favicon'] = '';
		empty($options['logo']) && $options['logo'] = '';
		empty($options['banner']) && $options['banner'] = '';

		empty($options['slider_duration']) && $options['slider_duration'] = '4000';
		empty($options['slider_tab_navigation']) && $options['slider_tab_navigation'] = 'no';
		empty($options['slider_prev_next_navigation']) && $options['slider_prev_next_navigation'] = 'yes';

		empty($options['slider_animation']) && $options['slider_animation'] = 'no';
		empty($options['image_animation']) && $options['image_animation'] = 'no';
		empty($options['section_animation']) && $options['section_animation'] = 'no';
		empty($options['section_animation_min_pixel']) && $options['section_animation_min_pixel'] = '0';

		empty($options['layout']) && $options['layout'] = 'boxed';
		empty($options['menu_sticky']) && $options['menu_sticky'] = 'no';
		empty($options['full_height']) && $options['full_height'] = 'no';

		empty($options['custom_javascript_code']) && $options['custom_javascript_code'] = '';

		return $options;
	}

	/**
	 *
	 */
	public static function themeOptionsPage(){
		echo FlowTemplateHelper::loadTemplate('adminsites', 'themeoptions', FlowFunctions::$themeOptions);
	}

	/**
	 * @param $input
	 *
	 * @return mixed
	 */
	public static function validateThemeOptions($input){

		!(is_numeric($input['slider_duration']) && ctype_digit($input['slider_duration'])) && $input['slider_duration'] = 4000;
		($input['layout'] != 'fullwidth') && $input['layout'] = 'boxed';
		($input['menu_sticky'] != 'yes') && $input['menu_sticky'] = 'no';
		($input['slider_tab_navigation'] != 'yes') && $input['slider_tab_navigation'] = 'no';
		($input['slider_prev_next_navigation'] != 'yes') && $input['slider_prev_next_navigation'] = 'no';

		return $input;
	}


	/**
	 *
	 */
	public static function addTaxonomyForObjectType(){
		register_taxonomy_for_object_type('category', 'testimonials');
		register_taxonomy_for_object_type('category', 'page');
		register_taxonomy_for_object_type('category', 'slider');
		register_taxonomy_for_object_type('post_tag', 'page');
	}

	public static function registerVendorCss(){

		if(is_admin()){
			return;
		}

		$themeUrl = get_template_directory_uri();


		wp_register_style('google-lato-font', 'https://fonts.googleapis.com/css?family=Lato:400,300italic,300,100italic,100,400italic,700,700italic,900,900italic');
		wp_enqueue_style('google-lato-font');

		wp_register_style('animate-min-css', $themeUrl . '/vendor/animate.min.css');
		wp_enqueue_style('animate-min-css');

		wp_register_style('bootstrap-min-css', $themeUrl . '/vendor/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('bootstrap-min-css');
	}
	/**
	 *
	 */
	public static function registerCss(){

		if(is_admin()){
			return;
		}

		$themeUrl = get_template_directory_uri();

		wp_register_style('flow-style-css', $themeUrl . '/style.css');
		wp_enqueue_style('flow-style-css');
	}

	/**
	 *
	 */
	public static function registerJavascript(){

		if(is_admin()){
			return;
		}

		$themeUrl = get_template_directory_uri();

		wp_enqueue_script('jquery');

		wp_register_script('bootstrap-min-js', $themeUrl . '/vendor/bootstrap/js/bootstrap.min.js');
		wp_enqueue_script('bootstrap-min-js');

		wp_register_script('flow-ready-js', $themeUrl . '/javascript/ready.js');
		wp_enqueue_script(('flow-ready-js'));
	}

	/**
	 *
	 */
	public static function registerAdminJavascript(){
		$jsSrc = get_template_directory_uri() . '/javascript/';
		wp_register_script('flow-admin-ready', $jsSrc . 'admin-ready.js', array('jquery', 'jquery-ui-dialog'), false);
		wp_enqueue_script('flow-admin-ready');
		wp_enqueue_media();
	}

	/**
	 *
	 */
	public static function registerPostTypes(){

		register_post_type('slider', array(
			'label'    => 'Slider',
			'public'   => false,
			'show_ui'  => true,
			'supports' => array(
				'title',
				'editor',
				'featured',
				'thumbnail',
				'post-thumbnails',
				'custom-fields',
				'revisions'
			)
		));

		register_post_type('testimonials', array(
			'label'    => 'Testimonials',
			'public'   => false,
			'show_ui'  => true,
			'supports' => array(
				'title',
				'editor',
				'thumbnail',
				'custom-fields',
			)
		));
	}

	/**
	 *
	 */
	public static function addMetaboxes(){

		add_meta_box('flow_slider_box', 'Select slider', 'FlowMetaboxes::showSlider', 'page', 'side', 'low');
		add_meta_box('flow_slider_box', 'Select slider', 'FlowMetaboxes::showSlider', 'post', 'side', 'low');

		add_meta_box('flow_banner_box', 'Banner options', 'FlowMetaboxes::showBanner', 'page', 'side', 'low');
		add_meta_box('flow_banner_box', 'Banner options', 'FlowMetaboxes::showBanner', 'post', 'side', 'low');

		add_meta_box('flow_banner_box', 'Slider options', 'FlowMetaboxes::showSliderOptions', 'slider', 'side', 'low');

		add_meta_box('tagsdiv-post_tag', 'Page Tags', 'post_tags_meta_box', 'page', 'side', 'low');
		add_meta_box('categorydiv', 'Categories', 'post_categories_meta_box', 'page', 'side', 'core');

		add_meta_box('flow_single_templates_box', 'Single Templates', 'FlowMetaboxes::showSingleTemplate', 'post', 'side', 'low');

		//add_meta_box('flow_themesettings_box', 'Theme settings', 'FlowMetaboxes::showThemeSettings', 'page', 'side', 'low');
		//add_meta_box('flow_themesettings_box', 'Theme settings', 'FlowMetaboxes::showThemeSettings', 'post', 'side', 'low');

	}

	/**
	 *
	 */
	public static function afterSetupTheme(){
		register_nav_menus(array('main-menu' => 'Main Menu'));
		load_theme_textdomain('flow-responsive', get_template_directory() . '/languages');
	}

	/**
	 *
	 */
	public static function removeFromWpHead(){
		global $wp_widget_factory;
		remove_action('wp_head', 'feed_links_extra', 3);
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'wp_generator');
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
		remove_action('wp_head', array(
			$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
			'recent_comments_style'
		));
	}

	/**
	 *
	 */
	public static function addPostTypeSupport(){
		add_post_type_support('slider', 'category');
		add_post_type_support('slider', 'excerpt');
		add_post_type_support('page', 'category');
		add_post_type_support('page', 'excerpt');
		add_post_type_support('testimonials', 'excerpt');
	}

	/**
	 *
	 */
	public static function preGetPosts($query){
		if(empty($query->query['post_type']) && is_category() || is_tag() && empty($query->query_vars['suppress_filters'])){
			$query->set('post_type', array(
				'post',
				'nav_menu_item',
				'page'
			));

			return $query;
		}
	}

	/**
	 * @param $class
	 *
	 * @return mixed
	 */
	public static function getAvatar($class){
		$class = str_replace("class='avatar", "class='author_gravatar circle ", $class);

		return $class;
	}

	/**
	 * @param $class
	 *
	 * @return string
	 */
	public static function searchForm($class){
		$form = '<div class="flow-search-form"><form role="search" method="get" class="search-form" action="' . esc_url(home_url('/')) . '">
					<input type="search" class="form-control" placeholder="' . esc_attr_x('Search &hellip;', 'placeholder') . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x('Search for:', 'label') . '" />
			</form></div>';

		return $form;
	}

	/**
	 * @param $content
	 *
	 * @return string
	 */
	public static function wrapContent($content){
		return $content;
	}

}

FlowFunctions::init();