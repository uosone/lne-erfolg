<?php
/*
Template Name: Sidebar left
*/
?><?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature'); ?>

	<div class="main main-sidebar-left">
		<div class="container">
			<div class="row">
				<?php if(is_active_sidebar('sidebar')) : ?>
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<div class="sidebar">
							<?php dynamic_sidebar('sidebar'); ?>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						<div class="content">
							<?php the_content(); ?>
						</div>
					</div>
				<?php else: ?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="content">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>