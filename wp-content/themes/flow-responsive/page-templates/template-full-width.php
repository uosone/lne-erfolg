<?php
/*
Template Name: Fullwidth
*/
?><?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature'); ?>
	<div class="main main-full-width">
		<div class="container-full-width">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>