jQuery(document).ready(function () {
    sliderBox();
    bannerBox();
    themeOptions();
});

themeOptions = function () {

    jQuery('.upload').click(function (e) {

        var button = this;

        e.preventDefault();

        var image = wp.media({
            title: 'Choose',
            multiple: false
        }).open()
            .on('select', function (e) {
                var url = image.state().get('selection').first().toJSON().url;
                targetId = jQuery(button).attr('data-target');
                jQuery('#' + targetId).attr('value', url);
            });
    });

    jQuery('.theme-options select').each(function () {
        jQuery(this).val(jQuery(this).attr('data-value'));
    });
};

bannerBox = function () {
    jQuery('#banner_image_url_upload').click(function (e) {

        e.preventDefault();

        var image = wp.media({
            title: 'Choose',
            multiple: false
        }).open()
            .on('select', function(e){
                var uploaded_image = image.state().get('selection').first();
                var image_url = uploaded_image.toJSON().url;
                jQuery('#button_image_url').val(image_url);
                jQuery('#banner_image').attr('src', image_url);
                jQuery('#banner_image').show();
                jQuery('#banner_image_url').attr('value', image_url);
            });
    });

    jQuery('#banner_remove_image').click(function(e){
        e.preventDefault();
        jQuery('#banner_image').hide();
        jQuery('#banner_image').attr('src', '');
        jQuery('#banner_image_url').attr('value', '');
    });
}

sliderBox = function(){

    jQuery("input[name='flow_slider_slides[]']").click(function () {
        var val = '';
        jQuery("input[name='flow_slider_slides[]']:checkbox:checked").each(function (i) {
            val += jQuery(this).val() + ',';
        });
        val = val.slice(0, -1);
        jQuery('#flow_slider').val(val);
    });

    if(jQuery('#flow_slider').length <= 0){
        return;
    }

    var val = jQuery('#flow_slider').val().split(',');

    jQuery("input[name='flow_slider_slides[]']").each(function (i) {
        if (jQuery.inArray(jQuery(this).val(), val) != -1) {
            jQuery(this).attr('checked', true);
        }
        else {
            jQuery(this).attr('checked', false);
        }
    });
}