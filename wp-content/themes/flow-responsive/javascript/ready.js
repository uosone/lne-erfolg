
jQuery(document).ready(function() {
    autoSqareImages();
    menu();
    xsMenu();
    sliderFullHeight();
    sliderBackgroundImage();
    imageOnMouseOverAnimation(themeOptions['image_animation']);
    sectionAnimation(themeOptions['section_animation'], themeOptions['section_animation_min_pixel']);
    modal();
    mouseOverBoxesResize();
    mouseOverBoxes();
    jQuery(window).resize(xsMenu);
    jQuery(window).resize(mouseOverBoxesResize);
    jQuery(window).resize(sliderFullHeight);
    jQuery(window).resize(autoSqareImages);
});

mouseOverBoxes = function(){


    turnUp = function(){
        jQuery(this).find('.text-wrapper').animate({ "top": "0px" }, "slow" );
    }

    turnDown = function(){
        newHeight = jQuery(this).height() - jQuery(this).find('h3').innerHeight();
        jQuery(this).find('.text-wrapper').animate({ "top": newHeight + "px" }, "fast" );
    };

    jQuery('.flow-mouse-over-boxes .item').hover(turnUp);
    jQuery('.flow-mouse-over-boxes .item').click(turnUp);

    jQuery('.flow-mouse-over-boxes .item').mouseleave(turnDown);
}

mouseOverBoxesResize = function(){

    jQuery('.flow-mouse-over-boxes .item').height(jQuery('.flow-mouse-over-boxes .item').width());

    height = jQuery('.flow-mouse-over-boxes .item').height();
    jQuery('.flow-mouse-over-boxes .text-wrapper').height(height);
    jQuery('.flow-mouse-over-boxes .text-wrapper').css('top', height-jQuery('.flow-mouse-over-boxes .item h3').innerHeight());

}

modal = function(){
    jQuery('.video-close').click(function() {
	parent = jQuery(this).parent().parent();
	iFrame = parent.find('iframe');
	iFrame.attr('src', iFrame.attr('src'));
    });
}

xsMenu = function(){

    if(jQuery(window).width() > 980) {
        jQuery('#main-menu').height(1);
        jQuery('html').css('overflow-y', 'auto');
        jQuery('body').css('overflow-y', 'auto');
        jQuery('html').css('overflow-x', 'hidden');
        jQuery('body').css('overflow-x', 'hidden');

        if(jQuery(window).scrollTop() < 60) {
            jQuery('.header').removeClass('header-not-transparent').addClass('header-transparent');
        }
    }

    return;

    var options = '<optgroup label="' + jQuery('.navbar-brand h1').text() + '" />';

    menu = jQuery('#main-menu li').each(function(){
        el = jQuery(this);
        link = el.find('a').first();

        if(el.hasClass('menu-item-has-children')){
            options += '<optgroup label="' + link.text().trim() + '" />';
        }
        else{
            options += '<option data-href="' + link.attr('href') + '">' + link.text().trim() + '</option>\n';
        }
    });

    jQuery('#mainMenuSelect').html('<select>'+options+'</select>');

    jQuery('#mainMenuSelect select').change(function(){
        window.location = jQuery(':selected',this).attr('data-href');
    });

}
/**
 *
 * @param animation
 */
imageOnMouseOverAnimation = function(animation) {

    if(animation == 'no' || animation == '') {
        return;
    }

    jQuery('a img').hover(function() {
        jQuery(this).addClass('animated ' + animation);
    });

    jQuery('a img').mouseleave(function() {
        jQuery(this).removeClass('animated ' + animation);
    });
}

/**
 *
 */
sectionAnimation = function(animation,minPixel) {

    if (jQuery(window).width() < minPixel) {
       return;
    }

    jQuery('.section').waypoint(function(direction) {

        el =  jQuery(this.element).find('.container');

        animationAttr = el.attr('data-animation');

        if(animationAttr && animationAttr.length > 0){
            el.addClass('animated ' + animationAttr);
        }
        else if(animation && animation != 'no' && animation.length > 0){
            el.addClass('animated ' + animation);
        }


    }, {
        offset: '99%'
    });
};

/**
 *
 */
sliderBackgroundImage = function() {
    jQuery.each(jQuery('#topSlider .item'), function(i, val) {
        jQuery(this).css('background-image', 'url(' + jQuery(this).find('img').attr('src') + ')').find('img').css('visibility', 'hidden');
    });
};

/**
 *
 */
sliderFullHeight = function() {

    slider = jQuery('.slider-full-height');

    if(slider.length <= 0) {
        return;
    }

    sliderTop = slider.position().top;
    windowHeight = jQuery(window).height();

    wpAdminBarHeight = 0;
    if(jQuery('#wpadminbar').length > 0) {
        wpAdminBarHeight = jQuery('#wpadminbar').height();
        jQuery('.header-fixed-top').css('top', wpAdminBarHeight);
    }

    marginTop = 0;
    if(jQuery('.wrapper-full-width').length > 0) {
        var marginTop = parseInt(jQuery('.wrapper-full-width').css('margin-top'));
    } else {
        var marginTop = parseInt(jQuery('.wrapper').css('margin-top'));
    }

    newSliderHeight = windowHeight - sliderTop - wpAdminBarHeight - marginTop;

    slider.height(newSliderHeight);
    caption = slider.find('.carousel-caption');
    slider.find('img').height(newSliderHeight);
};

/**
 *
 */
menu = function() {

    jQuery('.menu-icon').click(function(){
        if(!jQuery('.header').hasClass('toggle-is-open')){
            jQuery('.header').addClass('toggle-is-open');
            jQuery('html').css('overflow', 'hidden');
            jQuery('body').css('overflow', 'hidden');
            jQuery('.small-menu').html()
            jQuery('.header').removeClass('header-transparent').addClass('header-not-transparent');
            height = jQuery(window).height() - jQuery('.header').height();
            jQuery('#main-menu').height(height * 0.9);
            jQuery('#main-menu').css('margin-top', height * 0.05);
            jQuery('#main-menu').css('margin-bottom', height * 0.05);
        }
        else{
            jQuery('.header').removeClass('toggle-is-open');
            jQuery('html').css('overflow-y', 'auto');
            jQuery('body').css('overflow-y', 'auto');
            jQuery('html').css('overflow-x', 'hidden');
            jQuery('body').css('overflow-x', 'hidden');
            if(jQuery(window).scrollTop() < 60) {
                jQuery('.header').removeClass('header-not-transparent').addClass('header-transparent');
            }
        }

    });

    if(jQuery('.wrapper-full-width').length > 0) {
        var marginTop = parseInt(jQuery('.wrapper-full-width').css('margin-top'));
    } else {
        var marginTop = parseInt(jQuery('.wrapper').css('margin-top'));
    }

    if(!marginTop) {
        marginTop = 0;
    }

    if(jQuery('.feature-without-slider').length > 0) {
        jQuery('.header').addClass('header-not-transparent');
    }

    jQuery(window).scroll(function() {

            scroll = jQuery(window).scrollTop();

            if(scroll >= marginTop / 2) {
                jQuery('.wrapper').css('margin-top', 0);
            }
            else {
                jQuery('.wrapper').css('margin-top', marginTop);
            }

            if(scroll > 60 && jQuery('.feature-without-slider').length <= 0) {
                jQuery('.header-fixed-top').removeClass('header-transparent');
                jQuery('.header-fixed-top').addClass('header-not-transparent');
            }
            else if(!jQuery('.header-fixed-top').hasClass('toggle-is-open')) {
                jQuery('.header-fixed-top').removeClass('header-not-transparent');
                jQuery('.header-fixed-top').addClass('header-transparent');
            }

    });
};

/**
 *
 * @param options
 */
setHeaderSliderOptions = function(options) {

    jQuery('#' + options.id).carousel({
        interval: options.duration,
        pause: "hover"
    });

    jQuery('#' + options.id + '.carousel').bind('slid.bs.carousel', function(e) {
        slideTo = jQuery(e.relatedTarget).index();
        jQuery('#' + options.id + "_nav .nav li").removeClass('active');
        jQuery('#' + options.id + "_nav .nav li[data-slide-to='" + slideTo + "']").addClass('active');
    });
};

/**
 *
 */
autoSqareImages = function() {

    var images = jQuery('.flow-img-wrapper');

    images.each(function() {
        img = jQuery(this).find('img');
        img.height(img.width());
    });
};

/**
 *
 * @param selector
 */
createFilterItems = function(selector) {

    var selectorFilter = '#' + selector + '_filter ';
    var selector = '#' + selector + ' ';

    var items = jQuery(selector + '.row .item');
    var filteredItems = items.clone();
    var cols = jQuery(selector + ' .row .item:first').parent().attr('class');
    var maxItemsPerRow = jQuery(selector).attr('data-max-items-per-row');

    var containerClasses = jQuery(selector).attr('class');
    var lastFilter = 'all';

    jQuery(selectorFilter + 'a').click(function(event) {

        event.preventDefault();

        filter = jQuery(this).attr('data-filter');

        if(filter == lastFilter || (filter == 'all' && lastFilter == 'all')) {
            return;
        }

        lastFilter = filter;

        jQuery(selectorFilter + 'a').removeClass('active');
        jQuery(this).addClass('active');

        if(filter == 'all') {
            filteredItems = items.clone();
        } else {
            filteredItems = [];

            items.each(function() {
                tags = jQuery(this).attr('data-tags');

                if(!tags || tags == undefined || tags.indexOf(filter) == -1) {
                    return;
                }
                filteredItems.push(this);
            });
        }

        container = jQuery(selector);
        container.attr('class', containerClasses);
        container.html('');

        row = jQuery('<div class="row"/>');
        currentItems = 0;

        jQuery(filteredItems).each(function(index) {

            index++;
            currentItems++;

            colsContainer = jQuery('<div class="' + cols + '"/>');
            colsContainer.append(jQuery(this)).fadeIn(1000);
            row.append(colsContainer);

            if(currentItems == maxItemsPerRow || index == filteredItems.length) {
                container.append(row);
                row = jQuery('<div class="row"/>');
                currentItems = 0;
            }
        });
    });
};
