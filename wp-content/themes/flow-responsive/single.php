<?php

$selectedTemplate = FlowHelper::getMetaString(get_the_ID(), 'selected_template');

if(!$selectedTemplate){

	$content = FlowTemplateHelper::loadSingleTemplate('default');

	if($content){
		echo $content;
	}
}
else{

	$content = FlowTemplateHelper::loadSingleTemplate($selectedTemplate);

	if($content){
		echo $content;
	}
}
?>