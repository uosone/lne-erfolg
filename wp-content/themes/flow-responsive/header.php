<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <?php if (!empty(FlowFunctions::$themeOptions['favicon'])): ?>
        <link rel="shortcut icon" href="<?php echo FlowFunctions::$themeOptions['favicon']; ?>" type="image/png">
    <?php endif; ?>
    <title>
        <?php if (is_front_page()): ?>
            <?php bloginfo('name'); ?>
        <?php else: ?>
            <?php wp_title(''); ?>
        <?php endif; ?>
    </title>
    <script type="text/javascript">
        var themeOptions = <?php
            $themeOptions = FlowFunctions::$themeOptions;
            unset($themeOptions['custom_javascript_code']);
            echo json_encode($themeOptions);
            ?>;
    </script>
    <?php wp_head(); ?>
    <?php do_action('flow_header'); ?>

</head>
<body <?php body_class(); ?>>
<div class="wrapper<?php if (FlowFunctions::$themeOptions['layout'] == 'fullwidth'): ?> wrapper-full-width<?php endif; ?>">
    <div class="header <?php if (FlowFunctions::$themeOptions['menu_sticky'] == 'yes'): ?>header-fixed-top<?php endif; ?>">

        <div class="container">

            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">

                    <div class="logo">
                        <a href="<?php echo get_home_url(); ?>">
                            <?php if (!empty(FlowFunctions::$themeOptions['logo'])): ?>
                                <img alt="logo" src="<?php echo FlowFunctions::$themeOptions['logo'] ?>"/>
                            <?php else: ?>
                                <h1><?php echo get_bloginfo('name'); ?></h1>
                                <p><?php echo get_bloginfo('description'); ?></p>
                            <?php endif; ?>
                        </a>
                    </div>

                    <div class="menu-icon-wrapper">
                        <div class="menu-icon" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            Men&uuml;
                        </div>
                    </div>

                </div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <div class="main-menu-wrapper">
                        <?php
                        if (has_nav_menu('main-menu')) {
                            wp_nav_menu(array(
                                'echo' => true,
                                'menu_id' => 'main-menu',
                                'menu' => '',
                                'theme_location' => 'main-menu',
                                'depth' => 2,
                                'container' => false,
                                'menu_class' => 'nav navbar-nav navbar-right',
                                'fallback_cb' => 'wp_page_menu',
                                'walker' => new bootstrap_navwalker()
                            ));
                        }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
    </div>