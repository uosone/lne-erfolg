<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 footer-column-1">
                <?php if (is_active_sidebar('footer_column_1')) : ?>
                    <?php dynamic_sidebar('footer_column_1'); ?>
                <?php endif; ?>
            </div>

            <?php if (is_active_sidebar('footer_column_2')) : ?>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 footer-column-2">
                    <?php dynamic_sidebar('footer_column_2'); ?>
                </div>
            <?php endif; ?>

            <?php if (is_active_sidebar('footer_column_3')) : ?>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer-column-3">
                    <?php dynamic_sidebar('footer_column_3'); ?>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>
</div>
<?php wp_footer(); ?>

<script type="text/javascript">

    jQuery(document).ready(function () {
        <?php echo FlowTemplateHelper::getJavascriptFooter();?>
    });

    <?php if(!empty(FlowFunctions::$themeOptions['custom_javascript_code'])): ?>
    <?php echo FlowFunctions::$themeOptions['custom_javascript_code'];?>
    <?php endif;?>

    <?php
    $customJavascript = FlowHelper::getMetaString(get_the_ID(), 'custom_javascript');
    if (!empty($customJavascript)) {
        echo $customJavascript;
    }
    ?>

</script>

<script src="<?php echo get_template_directory_uri(); ?>/vendor/waypoints/jquery.waypoints.js"></script>

<?php do_action('flow_body_footer'); ?>

</body>
</html>
