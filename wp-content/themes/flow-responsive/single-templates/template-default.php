<?php
/*
single-template-name: Default
*/
?><?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature'); ?>

	<div class="main blog default main-sidebar-right">
		<div class="container">
			<div class="row">

				<?php if(is_active_sidebar('sidebar')) : ?>

					<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
						<div class="author">
							<?php echo get_avatar(get_the_author_meta('ID'), 128); ?><br>
							<?php echo get_the_author_link(); ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="content">
							<?php if(FlowFunctions::$globals['show_slider'] === true): ?>
								<h2>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
								</h2>
							<?php endif; ?>

							<?php the_content(); ?>
							<?php echo FlowTemplateHelper::getComments() ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="sidebar">
							<?php dynamic_sidebar('sidebar'); ?>
						</div>
					</div>

				<?php else: ?>
					<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
						<div class="content">
							<?php the_content(); ?>
							<?php echo FlowTemplateHelper::getComments() ?>
						</div>
					</div>
				<?php endif; ?>

			</div>

			<?php echo FlowTemplateHelper::getPaginationPreviousNextLinks() ?>
		</div>
	</div>
<?php endwhile; ?>

<?php endif; ?>
<?php get_footer(); ?>