<?php
/*
single-template-name: Fullwidth
*/
?><?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature'); ?>

	<div class="main blog fullwidth">
		<div class="container">
			<div class="row">

				<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
					<div class="author">
						<?php echo get_avatar(get_the_author_meta('ID'), 128); ?><br>
						<?php echo get_the_author_link(); ?>
					</div>
				</div>

				<div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
					<div class="content">
						<?php if(FlowFunctions::$globals['show_slider'] === true): ?>
							<h2>
								<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
							</h2>
						<?php endif; ?>
						<?php the_content(); ?>
						<?php echo FlowTemplateHelper::getComments() ?>
					</div>
				</div>

			</div>

			<?php echo FlowTemplateHelper::getPaginationPreviousNextLinks() ?>
		</div>
	</div>
<?php endwhile; ?>

<?php endif; ?>
<?php get_footer(); ?>