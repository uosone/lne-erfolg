<?php get_header(); ?>

<?php echo FlowTemplateHelper::loadTemplate('blocks', 'feature', array(
	'category_id'      => get_query_var('cat'),
	'disable_headline' => true
)); die('test');?>

<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
				<div class="content">
					<?php $ids = array(); ?>
					<?php if(have_posts()): ?>

						<?php
						while(have_posts()){
							the_post();
							$ids[] = get_the_ID();
						}

						echo FlowShortcodes::boxes(array(
							'template' => 'list',
							'source'   => 'ids',
							'ids'      => $ids
						));
						?>

						<?php echo FlowTemplateHelper::getPaginationList(); ?>

					<?php else: ?>
						<?php get_template_part('content', 'none'); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<div class="sidebar">
					<?php dynamic_sidebar('sidebar'); ?>
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>

