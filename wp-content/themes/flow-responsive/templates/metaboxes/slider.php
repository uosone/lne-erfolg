<div class="inside">
	<p><?php echo _e('Slider ids'); ?></p>
	<input id="flow_slider" class="form-input-tip" type="text" value="<?php echo esc_attr($vars['value']) ?>"
	       autocomplete="off" name="flow_slider">
	<table>
		<?php foreach($vars['slider'] as $slider): ?>
			<tr>
				<td><input type="checkbox" name="flow_slider_slides[]" value="<?php echo $slider['id'] ?>"/></td>
				<td><?php echo $slider['title'] ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</div>