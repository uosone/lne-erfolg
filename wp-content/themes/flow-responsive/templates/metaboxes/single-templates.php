<div class="inside">
	<p><?php echo _e('Templates'); ?></p>
	<select name="flow_selected_template" style="width:100%;">
	<?php foreach($vars['templates'] as $key => $name):?>
		<option <?php if(strtolower($vars['selected_template']) == strtolower($key)):?> selected <?php endif;?> value="<?php echo $key;?>"><?php echo $name;?></option>
	<?php endforeach;?>
	</select>
</div>