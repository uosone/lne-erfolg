<div class="inside">
	<p><?php echo _e('Slider animation'); ?><br>
	<select name="slider_options[animation]" style="width:100%;">
		<option value=""><?php echo _e('Default'); ?></option>
		<option value="no"><?php echo _e('No'); ?></option>
		<?php echo FlowTemplateHelper::getAnimationOptions($vars['animation']);?>
	</select></p>

	<input type="checkbox" name="slider_options[disable_title]" <?php if(!empty($vars['disable_title']) && $vars['disable_title'] == 'yes'):?> checked="checked" <?php endif;?> value="yes"/> <?php echo _e('Disable Headline');?><br><br>
	<input type="checkbox" name="slider_options[disable_content]" <?php if(!empty($vars['disable_content']) && $vars['disable_content'] == 'yes'):?> checked="checked" <?php endif;?> value="yes"/> <?php echo _e('Disable Text');?>
</div>