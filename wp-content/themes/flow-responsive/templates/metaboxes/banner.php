<div class="inside">

	<p><?php echo _e('Headline') ?><br>
	<input id="banner_input_headline" type="text" value="<?php echo esc_attr($vars['headline']) ?>"
	       name="banner[headline]">
	</p>

	<p><?php echo _e('Text') ?><br>
	<textarea id="banner_input_text" cols="20" rows="3"
	          name="banner[text]"><?php echo esc_attr($vars['text']) ?></textarea>
	</p>

	<input type="hidden" name="banner[image]" id="banner_image_url" value="<?php echo $vars['image']; ?>">

	<br><img src="<?php echo $vars['image']; ?>" id="banner_image"
	             style="width:100%;<?php if(empty($vars['image'])): ?>display:none;<?php endif; ?>"/>
	<br><input type="button" id="banner_image_url_upload" class=" button-secondary"
	           value="<?php echo _e('Upload banner') ?>">
	<br><br><a href="#" id="banner_remove_image"><?php echo _e('Remove'); ?></a>
	<br><br>
	<input type="checkbox" name="banner[disable_title]" <?php if(!empty($vars['disable_title']) && $vars['disable_title'] == 'yes'):?> checked="checked" <?php endif;?> value="yes"/> <?php echo _e('Disable Banner Headline');?><br><br>
	<input type="checkbox" name="banner[disable_content]" <?php if(!empty($vars['disable_content']) && $vars['disable_content'] == 'yes'):?> checked="checked" <?php endif;?> value="yes"/> <?php echo _e('Disable Banner Text');?>

	<p><?php echo _e('Banner animation'); ?><br>
		<select name="banner[animation]" style="width:100%;">
			<option value=""><?php echo _e('Default'); ?></option>
			<option value="no"><?php echo _e('No'); ?></option>
			<?php echo FlowTemplateHelper::getAnimationOptions($vars['animation']);?>
		</select></p>
</div>