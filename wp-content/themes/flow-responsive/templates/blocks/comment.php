<?php
switch($vars['comment']->comment_type) :
	case 'pingback' :
	case 'trackback' : ?>
		<li class="comment media" id="comment-<?php comment_ID(); ?>">
		<div class="media-body">
			<p>
				<?php _e('Pingback:'); ?><?php comment_author_link(); ?>
			</p>
		</div>
		<?php
		break;
	default :
		global $post; ?>

	<li class="comment media" id="li-comment-<?php comment_ID(); ?>">
		<a href="<?php echo $vars['comment']->comment_author_url; ?>" class="pull-left">
			<?php echo get_avatar($vars['comment']); ?>
		</a>

		<div class="media-body">
			<h4 class="media-heading comment-author vcard">
				<?php
				printf('<cite class="fn">%1$s %2$s</cite>', get_comment_author_link(), ($vars['comment']->user_id === $post->post_author) ? '<span class="label"> ' . __('Post author', 'flow-responsive') . '</span> ' : ''); ?>
			</h4>

			<?php if('0' == $vars['comment']->comment_approved) : ?>
				<p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></p>
			<?php endif; ?>

			<?php comment_text(); ?>

			<p class="meta">
				<?php printf('<a href="%1$s"><time datetime="%2$s">%3$s</time></a>', esc_url(get_comment_link($vars['comment']->comment_ID)), get_comment_time('c'), sprintf(__('%1$s at %2$s', 'flow-responsive'), get_comment_date(), get_comment_time())); ?>
			</p>

			<p class="reply">
				<?php comment_reply_link(array_merge($vars['args'], array(
						'reply_text' => __('Reply <span>&darr;</span>', 'flow-responsive'),
						'depth'      => $vars['depth'],
						'max_depth'  => $vars['args']['max_depth']
					))); ?>
			</p>

		</div>
		<?php
		break;
endswitch;