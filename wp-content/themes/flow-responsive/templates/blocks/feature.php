<?php

$slider = false;

if (!empty($vars['category_id'])) {
    $slider = FlowTemplateHelper::getSliderArray(null, $vars['category_id']);
} else {

    global $post;

    if ($post) {

        if (!$slider = FlowTemplateHelper::getSliderArray($post->ID)) {

            if ($image = FlowHelper::getMetaString($post->ID, 'banner_image')) {
                $slider[0]['image'] = $image;
            } else if (!empty(FlowFunctions::$themeOptions['banner'])) {
                $slider[0]['image'] = FlowFunctions::$themeOptions['banner'];
            }

            if (!empty($slider[0]['image']) && strlen($slider[0]['image']) > 2) {

                $animation = FlowHelper::getMetaString($post->ID, 'banner_animation');

                if (!empty($animation) && $animation != 'no') {
                    $slider[0]['animation'] = $animation;
                } else if ($animation != 'no') {
                    $slider[0]['animation'] = FlowFunctions::$themeOptions['slider_animation'];
                } else {
                    $slider[0]['animation'] = '';
                }


                if (FlowHelper::getMetaString($post->ID, 'banner_disable_title') != 'yes') {
                    $bannerHeadline = FlowHelper::getMetaString($post->ID, 'banner_headline');
                    (empty($bannerHeadline)) && $bannerHeadline = get_the_title();
                    $slider[0]['title'] = $bannerHeadline;
                } else {
                    $slider[0]['disable_title'] = true;
                }

                if (FlowHelper::getMetaString($post->ID, 'banner_disable_content') != 'yes') {
                    $bannerText = FlowHelper::getMetaString($post->ID, 'banner_text');
                    (empty($bannerText)) && $bannerText = get_the_excerpt();
                    $slider[0]['content'] = $bannerText;
                } else {
                    $slider[0]['disable_cotent'] = true;
                }

                if ($image = FlowHelper::getMetaString($post->ID, 'banner_image')) {
                    $slider[0]['image'] = $image;
                } else if (!empty(FlowFunctions::$themeOptions['banner'])) {
                    $slider[0]['image'] = FlowFunctions::$themeOptions['banner'];
                }
            }
        }
    }
}

if (count($slider) > 0) {
    FlowFunctions::$globals['show_slider'] = true;
}

?>
<div class="feature <?php if (count($slider) <= 0): ?> feature-no-slider<?php endif;?>">
    <?php if (count($slider) > 0): ?>
        <?php echo FlowTemplateHelper::buildSlider($slider, array('id' => 'topSlider', 'full_height' => FlowFunctions::$themeOptions['full_height'])); ?>
    <?php elseif (empty($vars['disable_title']) || $vars['disable_title'] == false): ?>
        <div class="container feature-without-slider">
            <h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
        </div>
    <?php endif; ?>
</div>
