<div class="widget flow-widget-show-entries">
	<h2 class="widgettitle">
		<?php echo $vars['title']; ?>
	</h2>
	<?php foreach($vars['entries'] as $entry): ?>
		<div class="row">
			<div class="item">
			   <a href="<?php echo $entry['href']; ?>">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
					<?php if(!empty($entry['image'])): ?>
					  <div class="flow-img-wrapper">
						  <img class="circle" src="<?php echo $entry['image'][0] ?>"/>
					  </div>
					<?php endif; ?>
				</div>
				<div class="col-xs-8 cols-sm-8 col-md-8 col-lg-8">
					<h4><?php echo $entry['title']; ?></h4>
					<p><?php echo $entry['text']; ?></p>
				</div>
			   </a>
			</div>
		</div>
	<?php endforeach; ?>
</div>



