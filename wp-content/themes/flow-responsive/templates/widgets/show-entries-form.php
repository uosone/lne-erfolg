<p>
	<label for="<?php echo $vars['this']->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
	<input class="widefat"
	       id="<?php echo $vars['this']->get_field_id('title'); ?>"
	       name="<?php echo $vars['this']->get_field_name('title'); ?>"
	       type="text"
	       value="<?php echo esc_attr($vars['title']); ?>"/>
</p>

<p><select class="widefat" id="<?php echo $vars['this']->get_field_id('data_type'); ?>"
        name="<?php echo $vars['this']->get_field_name('data_type'); ?>">

	<?php foreach($vars['data_types'] as $key => $dataType) : ?>
		<option <?php if(strtolower($vars['data_type']) == strtolower($key)):?> selected <?php endif;?> value="<?php echo esc_attr($key); ?>"><?php echo $dataType ?></option>
	<?php endforeach; ?>

</select></p>

<p><select class="widefat" id="<?php echo $vars['this']->get_field_id('menu_id'); ?>"
           name="<?php echo $vars['this']->get_field_name('menu_id'); ?>">

		<?php foreach($vars['menues'] as $key => $menu) : ?>
			<option <?php if(strtolower($vars['menu_id']) == strtolower($key)):?> selected <?php endif;?> value="<?php echo esc_attr($key); ?>"><?php echo $menu ?></option>
		<?php endforeach; ?>

	</select></p>

<p><label
		for="<?php echo $vars['this']->get_field_id('number_of_entries'); ?>"><?php _e('Number of posts to show:'); ?></label>
	<input id="<?php echo $vars['this']->get_field_id('number_of_entries'); ?>"
	       name="<?php echo $vars['this']->get_field_name('number_of_entries'); ?>"
	       type="text"
	       value="<?php echo $vars['number']; ?>" size="3"/></p>
