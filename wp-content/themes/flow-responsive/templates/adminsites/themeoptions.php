<div class="wrap theme-options">
	<?php screen_icon(); ?><h2>Theme options <?php bloginfo('name'); ?></h2>

	<?php if(isset($_REQUEST['settings-updated'])) : ?>
		<div class="updated fade">
			<p><strong><?php echo __('done', 'flow-responsive') ?></strong></p>
		</div>
	<?php endif; ?>

	<form method="post" action="options.php">
		<?php settings_fields('its_options'); ?>

		<table class="form-table">
			<tr valign="top">
				<th scope="row"><?php echo _e('Logo'); ?></th>
				<td><input style="width:100%;" id="its_theme_options_logo" class="regular-text" type="text"
				           name="its_theme_options[logo]" value="<?php esc_attr_e($vars['logo']); ?>"/></td>
				<td>
					<button data-target="its_theme_options_logo"
					        class="upload button-secondary"><?php echo _e('Upload logo'); ?></button>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><?php echo _e('Favicon'); ?></th>
				<td><input style="width:100%;" id="its_theme_options_favicon" class="regular-text" type="text"
				           name="its_theme_options[favicon]" value="<?php esc_attr_e($vars['favicon']); ?>"/></td>
				<td>
					<button data-target="its_theme_options_favicon"
					        class="upload button-secondary"><?php echo _e('Upload favicon'); ?></button>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Default banner'); ?></th>
				<td><input style="width:100%;" id="its_theme_options_banner" class="regular-text" type="text"
				           name="its_theme_options[banner]" value="<?php esc_attr_e($vars['banner']); ?>"/></td>
				<td>
					<button data-target="its_theme_options_banner"
					        class="upload button-secondary"><?php echo _e('Upload default banner'); ?></button>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Slider duration'); ?></th>
				<td><input style="width:100%;" id="its_theme_options_slider_duration" class="regular-text" type="text"
				           name="its_theme_options[slider_duration]"
				           value="<?php esc_attr_e($vars['slider_duration']); ?>"/></td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Slider tab navigation'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_slider_tab_navigation" class="regular-text"
					        type="text" name="its_theme_options[slider_tab_navigation]"
					        data-value="<?php esc_attr_e($vars['slider_tab_navigation']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<option value="yes"><?php echo _e('Yes'); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Slider prev/next navigation'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_slider_prev_next_navigation" class="regular-text"
					        type="text" name="its_theme_options[slider_prev_next_navigation]"
					        data-value="<?php esc_attr_e($vars['slider_prev_next_navigation']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<option value="yes"><?php echo _e('Yes'); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Slider animation'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_slider_animation" class="regular-text"
					        type="text" name="its_theme_options[slider_animation]"
					        data-value="<?php esc_attr_e($vars['slider_animation']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<?php echo FlowTemplateHelper::getAnimationOptions();?>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Image animation'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_image_animation" class="regular-text"
					        type="text" name="its_theme_options[image_animation]"
					        data-value="<?php esc_attr_e($vars['image_animation']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<?php echo FlowTemplateHelper::getAnimationOptions();?>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Section animation'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_section_animation" class="regular-text"
					        type="text" name="its_theme_options[section_animation]"
					        data-value="<?php esc_attr_e($vars['section_animation']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<?php echo FlowTemplateHelper::getAnimationOptions();?>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Disable section animation on devices with less Pixels than your entry'); ?></th>
				<td><input style="width:100%;" id="its_theme_options_section_animation_min_pixel" class="regular-text" type="text"
				           name="its_theme_options[section_animation_min_pixel]"
				           value="<?php esc_attr_e($vars['section_animation_min_pixel']); ?>"/></td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Layout'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_layout" class="regular-text" type="text"
					        name="its_theme_options[layout]" data-value="<?php esc_attr_e($vars['layout']); ?>">
						<option value="boxed"><?php echo _e('Boxed'); ?></option>
						<option value="fullwidth"><?php echo _e('Fullwidth'); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Full height slider'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_full_height" class="regular-text" type="text"
					        name="its_theme_options[full_height]"
					        data-value="<?php esc_attr_e($vars['full_height']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<option value="yes"><?php echo _e('Yes'); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Sticky menu'); ?></th>
				<td>
					<select style="width:100%;" id="its_theme_options_menu_sticky" class="regular-text" type="text"
					        name="its_theme_options[menu_sticky]"
					        data-value="<?php esc_attr_e($vars['menu_sticky']); ?>">
						<option value="no"><?php echo _e('No'); ?></option>
						<option value="yes"><?php echo _e('Yes'); ?></option>
					</select>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php echo _e('Custom javascript'); ?></th>
				<td><textarea style="width:100%;height:200px;" id="its_theme_options_custom_javascript_code"
				              class="regular-text" type="text"
				              name="its_theme_options[custom_javascript_code]"><?php esc_attr_e($vars['custom_javascript_code']); ?></textarea>
				</td>

			</tr>

		</table>

		<p class="submit"><input type="submit" class="button-primary" value="<?php echo _e('Save'); ?>"/></p>
	</form>
</div>