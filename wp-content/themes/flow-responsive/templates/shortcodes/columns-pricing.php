<?php
$i     = 0;
$x     = 0;
$count = count($vars['boxes']);
?>
<div <?php if(!empty($vars['id'])): ?>id="<?php echo $vars['id']; ?>"<?php endif; ?>
     data-max-items-per-row="<?php echo $vars['max']; ?>"
     class="flow-boxes flow-columns-pricing <?php if(!empty($vars['class'])): ?> <?php echo $vars['class']; ?><?php endif; ?>">
	<?php foreach($vars['boxes'] as $box): ?>

		<?php if($x == 0): ?>
			<div class="row">
		<?php endif; ?>

		<?php $i ++;
		$x ++; ?>

		<div class="<?php echo $vars['cols']; ?>">
			<div class="item">
				<div class="pricing-wrapper">

					<div class="title-wrapper" style="background-image:url('<?php echo $box['image'][0] ?>');">
						<h3>
							<?php if($box['href'] !== false): ?>
								<a href="<?php echo $box['href'] ?>">
									<?php echo $box['title']; ?>
								</a>
							<?php else: ?>
								<?php echo $box['title']; ?>
							<?php endif; ?>
						</h3>
					</div>

					<h4>
						<?php echo $box['excerpt']; ?>
					</h4>

					<div class="content">
						<?php echo $box['content']; ?>
					</div>

				</div>
			</div>
		</div>

		<?php if($x == $vars['max'] || $i == $count): ?>
			</div>
			<?php $x = 0; ?>
		<?php endif; ?>

	<?php endforeach; ?>

</div>