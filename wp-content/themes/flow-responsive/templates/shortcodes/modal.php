<div class="modal-shortcode<?php echo $vars['class']; ?>">

	<div class="<?php echo $vars['wrapper_link_class']; ?>">
		<a class="<?php echo $vars['link_class']; ?>" data-toggle="modal" data-target="#<?php echo $vars['id']; ?>">
			<?php echo $vars['link_text']; ?>
		</a>
	</div>

	<div class="modal fade" id="<?php echo $vars['id']; ?>" tabindex="-1" role="dialog"
	     aria-labelledby="<?php echo $vars['title']; ?>">
		<div class="modal-dialog <?php echo $vars['size'] ?>" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close <?php if(!empty($vars['video_modal'])):?> video-close <?php endif;?>" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="<?php echo $vars['id']; ?>Label"><?php echo $vars['modal_title']; ?></h4>
				</div>
				<div class="modal-body">
					<?php echo $vars['content']; ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary <?php if(!empty($vars['video_modal'])):?> video-close <?php endif;?>" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>