<div id="<?php echo $vars['id']; ?>" class="<?php echo $vars['class']; ?>">
	<ul class="nav nav-tabs nav-justified">
		<?php foreach($vars['tabs'] as $key => $tab): ?>
			<li class="<?php if($key == 0): ?>active<?php endif; ?>"><a
					href="#<?php echo $vars['id']; ?>_<?php echo $key; ?>"
					data-toggle="tab"><?php echo $tab['title']; ?></a></li>
		<?php endforeach; ?>
	</ul>

	<div class="tab-content">
		<?php foreach($vars['tabs'] as $key => $tab): ?>
			<div class="tab-pane <?php echo $tab['class']; ?><?php if($key == 0): ?>active<?php endif; ?>"
			     id="<?php echo $vars['id']; ?>_<?php echo $key; ?>"><?php echo $tab['content']; ?></div>
		<?php endforeach; ?>
	</div>
</div>