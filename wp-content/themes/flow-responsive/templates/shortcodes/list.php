<?php if(!empty($vars['tags'])): ?>

	<?php FlowTemplateHelper::addJavascriptFooter('createFilterItems(\'' . $vars['id'] . '\');'); ?>

	<div id="<?php echo $vars['id']; ?>_filter" class="flow-filter">
		<a href="#" data-filter="all" class="active">All</a>
		<?php foreach($vars['tags'] as $key => $tag): ?>
			<a data-filter="<?php echo $key; ?>" href="#"><?php echo $tag; ?></a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<div data-max-items-per-row="1" <?php if(!empty($vars['id'])): ?>id="<?php echo $vars['id']; ?>"<?php endif; ?>
     class="flow-list<?php if(!empty($vars['class'])): ?> <?php echo $vars['class']; ?><?php endif; ?>">

	<?php foreach($vars['boxes'] as $box): ?>

		<?php $content = !empty($box['excerpt'])? $box['excerpt']: strip_tags($box['content']); ?>

		<div class="row <?php echo $box['name']; ?>">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div
					class="item" <?php if(!empty($box['tags'])): ?> data-tags="<?php echo $box['tags']; ?>" <?php endif; ?>>
					<div class="col-xs-12 col-sm-4 col-md-3">
						<div<?php if(!empty($box['tags'])): ?> data-tags="<?php echo $box['tags']; ?>" <?php endif; ?>
							class="">
							<div class="post-thumbnail">
								<?php if(!empty($box['image']['0'])): ?>
							<?php if($box['href'] !== false): ?>
							  <div class="flow-img-wrapper">
								<a href="<?php echo $box['href'] ?>">
									<img class="<?php if(!empty($vars['image_class'])):?><?php echo $vars['image_class'];?><?php endif;?>" src="<?php echo $box['image'][0] ?>"/></a>
								</a>
							<?php else: ?>
									<img class="<?php if(!empty($vars['image_class'])):?><?php echo $vars['image_class'];?><?php endif;?>" src="<?php echo $box['image'][0] ?>" /></a>
							<?php endif; ?>
							</div>
						<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-8 col-md-9">
						<div class="">
							<h3>
								<?php if($box['href'] !== false): ?>
									<a href="<?php echo $box['href'] ?>">
										<?php echo $box['title'] ?>
									</a>

								<?php else: ?>
									<?php echo $box['title'] ?>
								<?php endif; ?>
							</h3>


							<p>
								<?php $text = $content; ?>
								<?php if(strlen($text) > 200): ?>
									<?php echo substr($text, 0, 200); ?> ...
								<?php else: ?>
									<?php echo $text; ?>
								<?php endif; ?>
							</p>

						</div>
						<?php if($box['href'] !== false): ?>
							<div class="read-more">
								<a class="btn btn-primary"
								   href="<?php echo $box['href'] ?>"><?php echo __('read more', 'flow-responsive') ?></a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>

		</div>
	<?php endforeach; ?>

	<?php if(!empty($vars['show_all_link'])): ?>
		<div class="show-all">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<a class="btn btn-primary btn-lg btn-block"
					   href="<?php echo $vars['show_all_link'] ?>"><?php echo __('show all', 'flow-responsive') ?></a>
				</div>
			</div>
		</div>
	<?php endif; ?>

</div>