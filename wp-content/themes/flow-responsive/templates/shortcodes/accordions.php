<div id="<?php echo $vars['id']; ?>" class="flow-accordions panel-group <?php echo $vars['class']; ?>">
	<?php foreach($vars['accordions'] as $key => $accordion): ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse"
					   data-parent="#<?php echo $vars['id']; ?>_<?php echo $key; ?>"
					   href="#<?php echo $vars['id']; ?>_<?php echo $key; ?>"
					   aria-controls="<?php echo $vars['id']; ?>_<?php echo $key; ?>">
						<?php echo $accordion['title']; ?>
					</a>
				</h4>
			</div>
			<div id="<?php echo $vars['id']; ?>_<?php echo $key; ?>" class="panel-collapse collapse">
				<div class="panel-body">
					<?php if(!empty($accordion['image'][0])):?>
					<div class="container">
						<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
							<div class="flow-img-wrapper">
								<img src="<?php echo $accordion['image'][0];?>"/>
							</div>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
							<?php echo $accordion['content']; ?>
						</div>
					</div>
					<?php else: ?>
						<?php echo $accordion['content']; ?>
			        <?php endif;?>

				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>