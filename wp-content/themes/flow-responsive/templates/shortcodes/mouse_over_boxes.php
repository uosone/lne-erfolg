<?php
$i = 0;
$x = 0;
$count = count($vars['boxes']);
?>

<div <?php if (!empty($vars['id'])): ?>id="<?php echo $vars['id']; ?>"<?php endif; ?>
     data-max-items-per-row="<?php echo $vars['max']; ?>"
     class="flow-mouse-over-boxes <?php if (!empty($vars['class'])): ?> <?php echo $vars['class']; ?><?php endif; ?>">
    <?php foreach ($vars['boxes'] as $key => $box): ?>

        <?php if ($x == 0): ?>
            <div class="row">
        <?php endif; ?>

        <?php
        $i++;
        $x++;
        $content = $box['content'];
        ?>


        <div class="<?php echo $vars['cols']; ?>">
            <div<?php if (!empty($box['tags'])): ?> data-tags="<?php echo $box['tags']; ?>" <?php endif; ?>
                    class="item">
                <img src="<?php echo $box['image'][0] ?>"/>
                <div class="text-wrapper" style="<?php echo FlowHelper::getMetaString($key, 'text_wrapper_css'); ?>">
                    <div class="block">
                        <?php if (!empty($box['title'])): ?>
                        <h3>
                            <?php if ($box['href'] !== false): ?>
                                <a href="<?php echo $box['href'] ?>">
                                    <?php echo $box['title']; ?>
                                </a>
                            <?php else: ?>
                                <?php echo $box['title']; ?>
                            <?php endif; ?>
                        </h3>
                    <?php endif; ?>

                    <?php if (!empty($content)): ?>
                        <p>
                            <?php echo $content; ?>
                        </p>
                    </div>

                        <?php if ($box['href'] !== false): ?>
                            <div class="read-more">
                                <a href="<?php echo $box['href'] ?>"><?php echo __('read more', 'flow-responsive') ?></a>
                            </div>
                        <?php endif; ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ($x == $vars['max'] || $i == $count): ?>
            </div>
            <?php $x = 0; ?>
        <?php endif; ?>

    <?php endforeach; ?>
</div>