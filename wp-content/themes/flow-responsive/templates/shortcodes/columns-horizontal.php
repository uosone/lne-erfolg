<?php
$i     = 0;
$x     = 0;
$count = count($vars['boxes']);
?>

<?php if(!empty($vars['tags'])): ?>

	<?php FlowTemplateHelper::addJavascriptFooter('createFilterItems(\'' . $vars['id'] . '\');'); ?>

	<div id="<?php echo $vars['id']; ?>_filter" class="flow-filter">
		<a href="#" data-filter="all" class="active">All</a>
		<?php foreach($vars['tags'] as $key => $tag): ?>
			<a data-filter="<?php echo $key; ?>" href="#"><?php echo $tag; ?></a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

<div <?php if(!empty($vars['id'])): ?>id="<?php echo $vars['id']; ?>"<?php endif; ?>
     data-max-items-per-row="<?php echo $vars['max']; ?>"
     class="flow-boxes flow-columns-horizontal <?php if(!empty($vars['class'])): ?> <?php echo $vars['class']; ?><?php endif; ?>">
	<?php foreach($vars['boxes'] as $box): ?>

		<?php if($x == 0): ?>
			<div class="row">
		<?php endif; ?>

		<?php
		 $i ++;
		 $x ++;
		$content = !empty($box['excerpt'])? $box['excerpt']: strip_tags($box['content']);
		?>

		<div class="<?php echo $vars['cols']; ?>">
			<div<?php if(!empty($box['tags'])): ?> data-tags="<?php echo $box['tags']; ?>" <?php endif; ?> class="item">
				<div class="container">
					<div class="row">
					 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<?php if(!empty($box['image'][0])): ?>
					<div class="flow-img-wrapper">
						<?php if($box['href'] !== false): ?>
							<a href="<?php echo $box['href'] ?>">
									<img class="<?php if(!empty($vars['image_class'])):?><?php echo $vars['image_class'];?><?php endif;?>" src="<?php echo $box['image'][0] ?>"></a>
							</a>
						<?php else: ?>
									<img class="<?php if(!empty($vars['image_class'])):?><?php echo $vars['image_class'];?><?php endif;?>" src="<?php echo $box['image'][0] ?>"></a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				</div>
				<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				 <?php if(!empty($box['title'])): ?>
					<h3>
						<?php if($box['href'] !== false): ?>
							<a href="<?php echo $box['href'] ?>">
								<?php echo $box['title']; ?>
							</a>
						<?php else: ?>
							<?php echo $box['title']; ?>
						<?php endif; ?>
					</h3>
				<?php endif; ?>

				<?php if(!empty($content)): ?>
					<p>
						<?php $text = $content ?>
						<?php if(strlen($text) > 200): ?>
							<?php echo substr($text, 0, 200); ?> ...
						<?php else: ?>
							<?php echo $text; ?>
						<?php endif; ?>
					</p>

					<?php if($box['href'] !== false): ?>
						<div class="read-more">
							<a href="<?php echo $box['href'] ?>"><?php echo __('read more', 'flow-responsive') ?></a>
						</div>
					<?php endif; ?>

				<?php endif; ?>
					</div>

					</div>
				</div>
			</div>
		</div>

		<?php if($x == $vars['max'] || $i == $count): ?>
			</div>
			<?php $x = 0; ?>
		<?php endif; ?>

	<?php endforeach; ?>
	<?php if(!empty($vars['show_all_link'])): ?>
		<div class="show-all">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<a class="btn btn-primary btn-block"
					   href="<?php echo $vars['show_all_link'] ?>"><?php echo __('show all', 'flow-responsive') ?></a>
				</div>
			</div>
		</div>
	<?php endif; ?>

</div>