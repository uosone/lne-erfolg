<div class="lne-desktop-menu">
    <nav>
        <?php
        if (has_nav_menu('lne-menu')) {
            wp_nav_menu(array(
                'echo' => true,
                'theme_location' => 'lne-menu',
                'depth' => 2,
                'container' => false,
                'fallback_cb' => 'wp_page_menu',
                'walker' => new lne_nav_walker()
            ));
        }
        ?>
    </nav>
</div>