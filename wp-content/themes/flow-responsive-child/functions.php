<?php


class FlowChildTheme
{

	public static function init(){
		require_once 'lne_nav_walker.php';
		add_action('after_setup_theme', 'FlowChildTheme::afterSetupTheme');
		add_filter('flow_load_template_blocks_feature', 'FlowChildTheme::filterFeature', 10, 2);
		add_filter('flow_load_template_slider_default', 'FlowChildTheme::filterSlider', 10, 2);
		add_action('flow_header', 'FlowChildTheme::actionHeader');
		add_action('init', 'FlowChildTheme::registerCss', 3);
	}

	public static function loadTemplate($template, $vars = array())
	{
		ob_start();
		require get_theme_root() . DS . 'flow-responsive-child' . DS . $template . '.php';
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	public static function registerCss()
	{
		wp_deregister_style('google-lato-font');
		wp_register_style('google-open-sans-font', 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic');
		wp_enqueue_style('google-open-sans-font');
		wp_enqueue_style('childtheme-style', get_stylesheet_directory_uri() . '/style.css', array('flow-style-css'));
	}

	public static function actionHeader(){
		$themeUrl = get_theme_root_uri() . '/flow-responsive-child/';
		echo '<script src="'.$themeUrl.'ready.js"></script>';
	}


	public static function filterFeature($content, $vars)
	{
		return self::loadTemplate('main-menu').$content;
	}

	public static function filterSlider($content, $vars){

		if($vars['options']['id'] != 'topSlider'){
			return $content;
		}

		return FlowChildTheme::loadTemplate('slider', $vars);

	}

	public static function afterSetupTheme(){
		register_nav_menus(array('lne-menu' => 'LNE Menu'));
	}
}

FlowChildTheme::init();
