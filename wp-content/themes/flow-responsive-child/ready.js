appendToMobileMenu = function () {

    jQuery('.lne-mobile-menu-item').remove();

    if (jQuery(window).width() > 980) {
        return;
    }

    lneMenu = jQuery('.lne-desktop-menu .top-menu-item');

    lneMenuHtml = '';
    jQuery(lneMenu).each(function (index) {
        link = jQuery(this).find('a');
        console.log(link[0].href);
        ul = jQuery(this).find('ul.sub-menu');
        lneMenuHtml += '<li class="dropdown lne-mobile-menu-item">\n' + '<a href="' + link[0].href + '" data-toggle="false" class="dropdown-toggle">' + link.html() + '<span class="caret"></span></a>\n' + '<ul role="menu" class="dropdown-menu">' + ul.html() + '</ul>\n</li>\n\n';
    });

    jQuery('#main-menu').append(lneMenuHtml);
};

topSliderMouseOver = function () {

    slider = jQuery('#topSlider .item');

    jQuery(slider).each(function () {
        jQuery(this).hover(function () {
            jQuery(this).css('background-image', 'url(' + jQuery(this).attr('data-hover') + ')');

        });

        jQuery(this).mouseleave(function () {
            jQuery(this).css('background-image', 'url(' + jQuery(this).attr('data-background') + ')').fadeIn('slow');
        });
    });
};

positionBackgroundImage = function () {
    position = jQuery('.section .container').position();
}

jQuery(document).ready(function () {
    positionBackgroundImage();
    appendToMobileMenu();
    jQuery(window).resize(appendToMobileMenu);
});

jQuery(document).ready(function () {
    var linksTopMenu = document.querySelectorAll('.dropdown-toggle-a');
    for (var x = 0; x < linksTopMenu.length; x++) {
        linksTopMenu[x].setAttribute('data_url', linksTopMenu[x].href);
        linksTopMenu[x].href = '#';
    }
});

document.addEventListener('DOMContentLoaded', run);

function run() {
    document.querySelector('.menu-icon').addEventListener('click', mobileMenu);
}

function mobileMenu() {
    var linksTopMenu = document.querySelectorAll('.dropdown-toggle-a');
    for (var x = 0; x < linksTopMenu.length; x++) {
        linksTopMenu[x].href = linksTopMenu[x].getAttribute('data_url');
        linksTopMenu[x].setAttribute('data-toggle', 'false');
    }
}