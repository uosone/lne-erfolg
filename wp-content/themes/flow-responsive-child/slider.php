<?php $countSlider = count($vars['slider']); ?>

<?php
FlowTemplateHelper::addJavascriptFooter('setHeaderSliderOptions(' . json_encode($vars['options'], true) . ');');
?>

<div id="<?php echo $vars['options']['id']; ?>"
     class="carousel slide
	     <?php if(!empty($vars['options']['class'])): ?> <?php echo $vars['options']['class'] ?><?php endif; ?>
	     <?php if(!empty($vars['options']['full_height']) && $vars['options']['full_height'] == 'yes'): ?> slider-full-height <?php endif; ?>"
     data-ride="carousel">

    <div class="carousel-inner" role="listbox">
        <?php for($i = 0; $i < $countSlider; $i ++): ?>
            <div data-background="<?php echo $vars['slider'][ $i ]['image'] ?>" data-hover="<?php echo FlowHelper::getMetaString($vars['slider'][ $i ]['id'], 'hover_slider')?>"
                <?php if(!empty($vars['slider'][ $i ]['id'])): ?> id="slider-<?php echo $vars['slider'][ $i ]['id'] ?>" <?php endif; ?>
                class="item <?php if($i == 0): ?>active<?php endif; ?><?php if(!empty($vars['slider'][ $i ]['class'])): ?> <?php echo $vars['slider'][ $i ]['class']; ?> <?php endif; ?>">
                <?php if(!empty($vars['slider'][ $i ]['link'])): ?>
                <a href="<?php echo $vars['slider'][ $i ]['link'] ?>">
                    <?php endif; ?>

                    <?php if(!empty($vars['slider'][ $i ]['image'])): ?>
                        <img src="<?php echo $vars['slider'][ $i ]['image'] ?>"
                        alt="<?php if(!empty($vars['slider'][ $i ]['title'])): ?><?php echo $vars['slider'][ $i ]['title']; ?><?php endif; ?>"/>

                    <?php endif; ?>

                    <?php if($vars['options']['type'] == 'content'): ?>
                        <?php echo $vars['slider'][ $i ]['content'] ?>
                    <?php else: ?>
                        <div class="lne-caption <?php if(!empty($vars['slider'][ $i ]['animation']) && $vars['slider'][ $i ]['animation'] != 'no'):?>
						?> animated <?php echo $vars['slider'][ $i ]['animation']; ?><?php endif;?>">
                                <?php if(!empty($vars['slider'][ $i ]['title']) && (empty($vars['slider'][ $i ]['disable_title']) || $vars['slider'][ $i ]['disable_title'] != 'yes')): ?>
                                    <h3><?php echo $vars['slider'][ $i ]['title'] ?></h3>
                                <?php endif; ?>
                                <?php if(!empty($vars['slider'][ $i ]['content']) && (empty($vars['slider'][ $i ]['disable_content']) || $vars['slider'][ $i ]['disable_content'] != 'yes')): ?>
                                    <p><?php echo $vars['slider'][ $i ]['content'] ?></p>
                                <?php endif; ?>

                        </div>
                    <?php endif; ?>

                    <?php if(!empty($vars['slider'][ $i ]['link'])): ?>
                </a>
            <?php endif; ?>

            </div>
        <?php endfor; ?>

        <?php if($countSlider > 1 && $vars['options']['type'] != 'content' && (empty($vars['options']['tab_navigation']) || $vars['options']['tab_navigation'] !== 'no')): ?>
            <div id="<?php echo $vars['options']['id']; ?>_nav" class="slider-nav">
                <ul class="nav nav-pills nav-justified">
                    <?php for($i = 0; $i < $countSlider; $i ++): ?>
                        <li data-target="#<?php echo $vars['options']['id']; ?>" data-slide-to="<?php echo $i; ?>"
                            class="<?php if($i == 0): ?>active<?php endif; ?>">
                            <a href="#">

                                <?php if(!empty($vars['slider'][ $i ]['title'])): ?>
                                    <span class="title">
									<?php echo $vars['slider'][ $i ]['title']; ?>
								</span>
                                <?php endif; ?>

                                <small>
                                    <?php if(!empty($vars['slider'][ $i ]['excerpt'])): ?>
                                        <?php echo $vars['slider'][ $i ]['excerpt'] ?>
                                    <?php endif; ?>
                                </small>
                            </a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if($countSlider > 1): ?>

            <?php if(empty($vars['options']['prev_next_links']) || $vars['options']['prev_next_links'] !== 'no'): ?>
                <a class="left carousel-control" href="#<?php echo $vars['options']['id']; ?>" role="button"
                   data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>
                <a class="right carousel-control" href="#<?php echo $vars['options']['id']; ?>" role="button"
                   data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>